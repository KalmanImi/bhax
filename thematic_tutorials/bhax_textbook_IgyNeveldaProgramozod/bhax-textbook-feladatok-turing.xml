<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Turing!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
<para>
Videólink a programok futtatásáról:
<link xlink:href="https://youtu.be/KUAtGcdsOEg">https://youtu.be/KUAtGcdsOEg</link>
</para>

    <section>
        <title>Végtelen ciklus</title>
        <para>
			A csokor első feladata olyan végtelen ciklusok megírását célozza meg, amelyek során egy processzormagot dolgoztatunk 0 - illetve 100 százalékban, illetve minden magot 100 százalékban.
        </para>
		<para>
			Általában a végtelen ciklus kerülendő, de vannak olyan esetek is, amikor ez a célunk. Ilyen például egy szerverfolyamat futása. A magam részéről - a villámvadászat támogatására - olyan Python szkriptet írtam, amely adott időközönként a lightningmaps.org oldaláról információkat gyűjt a villámlásokról. Amennyiben egy adott távolságon belül érzékel a villámlokalizációs rendszer villámlást, és az megjelenik a térképen, a program hangjelzést generál. Így kisebb eséllyel maradok le a közeli villámlásokról. Ezen program megírásánál végtelen ciklust alkalmaztam, hogy a program futása akár az éjszakai időszakban is akadálytalan legyen, beavatkozás nélkül. 
		</para>
		<para>
			Visszatérve a feladatra, először egy olyan C nyelvű kódcsipetet szeretnék demonstrálni, amely egy magot dolgoztat 100 százalékban:
		</para>
		<programlisting language="c"><![CDATA[

int main () {
  
	for (;;);

	return 0;
}
]]>
        </programlisting>

		<para>
			A programot meg lehet írni <literal>while</literal> utasítással is, de célszerű a hagyományos <literal>for(;;)</literal> formát használni, hordozhatósága és egyértelműsége miatt.
		</para>
<para>
Nézzünk erre egy fordítást, futtatást:
</para>

<figure>
<title>Végtelen ciklus végrehajtása egy magon, ütemezés nélkül</title>
<mediaobject>
<imageobject>
<imagedata fileref="img/turing_egymag100.png"/>
</imageobject>
<textobject>
<phrase>Végtelen ciklus végrehajtása egy magon, ütemezés nélkül</phrase>
</textobject>
</mediaobject>
</figure>




		<para>
			A következő programcsipet 0 százalékban dolgoztat egy magot:
		</para>
		<programlisting language="c"><![CDATA[#include <unistd.h>
int main () {
  for (;;)
    sleep(1);
    
  return 0;
}]]>
        </programlisting> 
		<para>
			Ilyen formában lehet ütemezni is. Itt jól látható lesz, hogy alapértelmezetten nem dolgozik egyetlen mag sem a program részéről, csak adott periódusonként. Ha a ciklusban számításigényes rész lenne megírva, periodikusan megnövekedne az egyik processzormag görbéje. Mivel ebben a kódcsipetben a számításigény közelít a nullához, nem lesz megfigyelhető a jelenség, olyan mintha a program nem is futna:
		</para>

<figure>
<title>Végtelen ciklus végrehajtása egy magon, ütemezéssel</title>
<mediaobject>
<imageobject>
<imagedata fileref="img/turing_egymag0.png"/>
</imageobject>
<textobject>
<phrase>Végtelen ciklus végrehajtása egy magon, ütemezéssel</phrase>
</textobject>
</mediaobject>
</figure>


<para>
Itt fontos az unistd.h header beinkludálása az ütemezés miatt. Most nézzünk egy olyan példát, ami 100 százalékon pörget minden magot:
		</para>
		<programlisting language="c"><![CDATA[#include <omp.h>


int main () {
#pragma omp parallel
{
  for (;;);
}
  return 0;
}
]]>
        </programlisting>        



<figure>
<title>Minden mag teljes erőbedobással dolgozik</title>
<mediaobject>
<imageobject>
<imagedata fileref="img/turing_tobbmag100.png"/>
</imageobject>
<textobject>
<phrase>Minden mag teljes erőbedobással dolgozik</phrase>
</textobject>
</mediaobject>
</figure>
<para>
Ehhez az omp header beinkludálására van szükség, illetve fordításnál jelezni kell, hogy párhuzamosítást alkalmazunk, ezt a -fopenmp kapcsolóval érhetjük el.
</para>


		<para>
		És végül a Python szkript, amely figyelmeztet közeli villámlás esetén:
		</para>
		<programlisting language="Python"><![CDATA[
import urllib.request
from PIL import Image
import os
from time import gmtime, strftime
import time
import winsound

frequency = 500
duration = 300

pos1 = 100
pos2 = 124

#Debi: 143,89,8
x = ['143']
y = ['89']

while True:
    for i in x:
        for j in y:
            #print(i,j)
            link = 'https://tiles.lightningmaps.org/?x='+i+'&y='+j+'&z=8&s=256&t=5&T=12992604'
            file = i+j+'.png'
            urllib.request.urlretrieve(link,file)
            im = Image.open(file)
            pix = im.load()
            n = 0
            for k in range(pos1-100,pos1+101):
                for l in range(pos2-100,pos2+101):
                    if pix[k,l] ==5 or pix[k,l] == 6:
                        n = n+1
                    if k == pos1-100 or k == pos1+100 or l == pos2-100 or l == pos2+100:
                        pix[k,l] = 8
                    elif k == pos1 and l == pos2:
                        pix[k,l] = 8
            if n != 0:
                for k in range(1,5):
                    winsound.Beep(frequency*k, duration)
            print(strftime("%Y-%m-%d %H:%M:%S", gmtime())+'   '+i+'-'+j+': '+str(n))
            im.save(i+j+'.png')
            im.close()
            #os.remove(file)
    time.sleep(120)

        ]]></programlisting>  
		
		<para>
		A forráskódban megjelenik a <literal>while True</literal> utasítás, illetve a végén a <literal>time.sleep(120)</literal>, amelyek állandó, ütemezett futást generálnak. Avagy 120 másodpercenként kerül letöltésre a debreceni térképkivágat a villámokkal, és kerül vizsgálat alá. Ezt a szkriptet elindítva esélytelen, hogy lemaradjunk bármely zivatareseményről Debrecen térségében. Ráadásul a lightningmaps.org oldalon, ahonnan a képek származnak a villámok közel real time felkerülnek a térképre, így mindig aktuális a figyelmeztetés. A szkript Windows környezetben akadálymentesen lefut, Linux alatt a hangjelzéses rész átírandó.
		</para>

<figure>
<title>Egy kivágat, melyre figyelmeztetést kaptunk</title>
<mediaobject>
<imageobject>
<imagedata fileref="img/turing_villam.png"/>
</imageobject>
<textobject>
<phrase>Egy kivágat, melyre figyelmeztetést kaptunk</phrase>
</textobject>
</mediaobject>
</figure>

<para>
A középső pont jelöli Debrecent a kivágatban, a színes pontok a villámlások helyeit, a négyzet a figyelmeztetés tartományát.
</para>



        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/lvmi6tyz-nI">https://youtu.be/lvmi6tyz-nI</link>
        </para>
        <para>
            Megoldás forrása: 

<link xlink:href="Turing/egymag0.c">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/egymag0.c</filename>
</link>,
<link xlink:href="Turing/egymag.c">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/egymag.c</filename>
</link>,
<link xlink:href="Turing/tobbmag.c">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/tobbmag.c</filename>
</link>,
<link xlink:href="Turing/alarmDebi.py">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/alarmDebi.py</filename>
</link>.
        </para>
                                
        <tip>
            <title>Werkfilm</title>
            <itemizedlist>
                <listitem>
                    <para>
                        <link xlink:href="https://youtu.be/lvmi6tyz-nI">https://youtu.be/lvmi6tyz-nI</link>
                    </para>    
                </listitem>                
            </itemizedlist>                
        </tip>
    </section>        
        
    <section>
        <title>Lefagyott, nem fagyott, akkor most mi van?</title>
        <para>
            Mutasd meg, hogy nem lehet olyan programot írni, amely bármely más programról eldönti, hogy le fog-e fagyni vagy sem!
        </para>
        <para>
            Megoldás videó:
        </para>
        <para>
            Megoldás forrása:  tegyük fel, hogy akkora haxorok vagyunk, hogy meg tudjuk írni a <function>Lefagy</function>
            függvényt, amely tetszőleges programról el tudja dönteni, hogy van-e benne végtelen ciklus:              
        </para>
        <programlisting language="c"><![CDATA[Program T100
{

	boolean Lefagy(Program P)
	{
		 if(P-ben van végtelen ciklus)
			return true;
		 else
			return false; 
	}

	main(Input Q)
	{
		Lefagy(Q)
	}
}]]></programlisting>            
        <para>
            A program futtatása, például akár az előző <filename>v.c</filename> ilyen pszeudókódjára:
            <screen><![CDATA[T100(t.c.pseudo)
true]]></screen>            
            akár önmagára
            <screen><![CDATA[T100(T100)
false]]></screen>  
            ezt a kimenetet adja.          
        </para>
        <para>
            A T100-as programot felhasználva készítsük most el az alábbi T1000-set, amelyben a
            Lefagy-ra épőlő Lefagy2 már nem tartalmaz feltételezett, csak csak konkrét kódot:
        </para>
        <programlisting language="c"><![CDATA[Program T1000
{

	boolean Lefagy(Program P)
	{
		 if(P-ben van végtelen ciklus)
			return true;
		 else
			return false; 
	}

	boolean Lefagy2(Program P)
	{
		 if(Lefagy(P))
			return true;
		 else
			for(;;); 
	}

	main(Input Q)
	{
		Lefagy2(Q)
	}

}]]></programlisting>            
        <programlisting><![CDATA[]]></programlisting>            
        <para>
            Mit for kiírni erre a <computeroutput>T1000(T1000)</computeroutput> futtatásra?
                                
            <itemizedlist>
                <listitem>
                    <para>Ha T1000 lefagyó, akkor nem fog lefagyni, kiírja, hogy true</para>                        
                </listitem>
                <listitem>
                    <para>Ha T1000 nem fagyó, akkor pedig le fog fagyni...</para>                        
                </listitem>
            </itemizedlist>
            akkor most hogy fog működni? Sehogy, mert ilyen <function>Lefagy</function>
            függvényt, azaz a T100 program nem is létezik.                
        </para>
    </section>        
                
    <section>
        <title>Változók értékének felcserélése</title>
        <para>
            Írj olyan C programot, amely felcseréli két változó értékét, bármiféle logikai utasítás vagy kifejezés
            használata nélkül!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://bhaxor.blog.hu/2018/08/28/10_begin_goto_20_avagy_elindulunk">https://bhaxor.blog.hu/2018/08/28/10_begin_goto_20_avagy_elindulunk</link>
        </para>
        <para>
            Megoldás forrása:
<link xlink:href="Turing/valtozocsere.c">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/valtozocsere.c</filename>
</link>
<programlisting language="c"><![CDATA[Program Valtozocsere
{
#include <stdio.h>

int main() {

	int a = 10;
	int b = 20;
	
	printf("%d %d\n",a,b);

	a = a + b;
	b = a - b;
	a = a - b;

	
	printf("%d %d\n",a,b);

	return 0;

} 
}]]>
        </programlisting>
        </para>
        <para>
            A programkódot többféle matematikai eljárás alkalmazásával is megírhatjuk. Jelen esetben az összeadás-kivonás műveleteket alkalmaztam, de hasonló eredményt kapok, ha összeadás helyett szorzok, kivonás helyett osztok. Továbbá megoldható a változócsere problémája kizáró vagy logikai utasítással is.
        </para>
<figure>
<title>Változók felcserélése</title>
<mediaobject>
<imageobject>
<imagedata fileref="img/turing_valtozocsere.png"/>
</imageobject>
<textobject>
<phrase>Változók felcserélése</phrase>
</textobject>
</mediaobject>
</figure>



    </section>                     

    <section>
        <title>Labdapattogás</title>
        <para>
            Először if-ekkel, majd bármiféle logikai utasítás vagy kifejezés
            nasználata nélkül írj egy olyan programot, ami egy labdát pattogtat a karakteres konzolon! (Hogy mit értek
            pattogtatás alatt, alább láthatod a videókon.)
        </para>

<caution>
<title>Tutoriáltam: Salánki László</title>
<para></para>
</caution>

        <para>
            Megoldás videó: <link xlink:href="https://bhaxor.blog.hu/2018/08/28/labdapattogas">https://bhaxor.blog.hu/2018/08/28/labdapattogas</link>
        </para>
        <para>
			Megoldás forrása:
<link xlink:href="Turing/labdapattogas.c">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/labdapattogas.c</filename>
</link>

            <programlisting language="c"><![CDATA[Program Labdapattogas
{
#include <stdio.h>
#include <curses.h>
#include <unistd.h>
#include <math.h>	//Kell az abszolút érték kiszámolásához

int main ( void )
{
    WINDOW *ablak;
    ablak = initscr ();

    int x = -1;		//Táblán kívül esik 
    int y = -1;		// -||-
	int n = 1;		//Ezen a természetes számon végzem a maradékos osztást

    int mx;		//Maxi érték x részéről
    int my;		//Maxi érték y részéről

    for ( ;; ) {

        getmaxyx ( ablak, my , mx );	//Lekérem a maxi értékeket

        mvprintw ( y, x, "O" );		//Kiírom x-et és y-t

        refresh ();		
        usleep ( 100000 );
		
		x = abs((n%((mx-1)*2))-(mx-1));		//x koordinátát megadom
		y = abs((n%((my-1)*2))-(my-1));		//y koordinátát megadom
		n = n + 1;	//növelem minden 'képkockánál' n értékét
    }

    return 0;
}
}]]></programlisting>  
        </para>
        <para>
            A feladat megoldásához először át kell gondolni, hogy valójában milyen matematikai folyamat zajlik a háttérben. Ilyen feladatoknál célszerű rajzot is készíteni, én így tettem. Lerajzoltam a labda várható mozgását, és megfigyeltem, hogy egy elképzelt koordináta-rendszer x- és y tengelyén mi történik a labda mozgása során. Miután észrevettem, hogy a labdának mind az x-, mind pedig az y tengelyen oda-vissza kell lépegetni az ablak méretének függvényében, világossá vált, hogyan lehet megírni a program if nélküli változatát. Első próbálkozásom a karakter és integer típus közötti típuskonverzión alapult, ám rájöttem, hogy ennél sokkal elegánsabb és egyszerűbb kód is megírható. A képlet, mely rendre megadja a koordinátákat:
	x = abs((n%((mx-1)*2))-(mx-1)),
ahol mx az ablak hossza,
n integer típus, mely egyenként lépeget 1-től fölfelé.
mx értékéből ki kell vonni 1-et, ugyanis a labda legkisebb koordinátája a 0, így a golyó a maximumnál kilépne a képből. Továbbá a maradékképzés alanyát szorzom 2-vel, ugyanis eltolom az egész maradékot mx-1-el balra. Így, ha n-et növelem és a maradék eltoltjának abszolút értékét veszem, mindig a megfelelő eredményt (egy oda-vissza lépegető sorozatot) kapom. 
</para>
<para>
A program fordításánál a -lcurses kapcsolót kell használni, máskülönben a fordítás sikertelen lesz. Ezzel jelezzük ugyanis a fordítónak, hogy a curses library tartalmát is alkalmazzuk a programcsipetben. A végeredmény:
</para>

<figure>
<title>A labdapattogás program futás közben</title>
<mediaobject>
<imageobject>
<imagedata fileref="img/turing_labdapattogas.png"/>
</imageobject>
<textobject>
<phrase>A labdapattogás program futás közben</phrase>
</textobject>
</mediaobject>
</figure>


        
    </section>                     

    <section>
        <title>Szóhossz és a Linus Torvalds féle BogoMIPS</title>
        <para>
            Írj egy programot, ami megnézi, hogy hány bites a szó a gépeden, azaz mekkora az <type>int</type> mérete.
            Használd ugyanazt a while ciklus fejet, amit Linus Torvalds a BogoMIPS rutinjában! 
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/9KnMqrkj_kU">https://youtu.be/9KnMqrkj_kU</link>, 
            <link xlink:href="https://youtu.be/KRZlt1ZJ3qk">https://youtu.be/KRZlt1ZJ3qk</link>, 
            <link xlink:href=""></link>.
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Turing/szohossz.c">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/szohossz.c</filename>
            </link>
        </para>
		<para>
			A szóhossz-vizsgáló program:
		</para>
		<programlisting language="c"><![CDATA[Program Szohossz
{
#include <stdio.h>

int main() {

	int i = 1;
	int bit = 0;

	while (i != 0) {
		i <<= 1;
		bit+=1;
	}

	printf("A szóhossz: %d bit.\n", bit);
	return 0;
}]]>
        </programlisting>

        <para>
            A BogoMIPS a következő szavakból tevődik össze: "bogus" + "MIPS" (Millions of instructions per second), ahol a bogus hamisat jelent, az utóbbi a processzor-sebesség egy mérőszáma. Azaz egy durva becslést lehet a program segítségével nyerni a processzor teljesítményére. A 'durva' jelző erősségére a bogus szóból származó Bogo előtag utal.
        </para>
		<para>
            A kódban szerepel egy olyan rész is, ami a gépi szóhossz kiszámolására alkalmazható, kisebb átalakítással. A szóhossz (integer típus) kiszámolásának programkódja csupán egy integer típusú változóból áll, melynek az 1 értéket adjuk meg, illetve egy integer típusú bitszámolóból. A példában azt vizgsáljuk, hogy mennyivel kell eltolni a integerben szereplő '1' bitet (bináris ábrázolás mellett), hogy végül 0-t kapjak az integer értékére. Eredményként 32-t kaptam. 
        </para>

<figure>
<title>Gépi szó hosszának meghatározása</title>
<mediaobject>
<imageobject>
<imagedata fileref="img/turing_szohossz.png"/>
</imageobject>
<textobject>
<phrase>Gépi szó hosszának meghatározása</phrase>
</textobject>
</mediaobject>
</figure>


    </section>                     

    <section>
        <title>Helló, Google!</title>
        <para>
            Írj olyan C programot, amely egy 4 honlapból álló hálózatra kiszámolja a négy lap Page-Rank 
            értékét!
        </para>
        <para>
            Megoldás videó: <link xlink:href=""></link>
        </para>
        <para>
            Megoldás forrása: 
<link xlink:href="Turing/pagerank.cpp">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/pagerank.cpp</filename>
</link>

        </para>
		<programlisting language="c"><![CDATA[Program pagerank
{
#include <stdio.h>
#include <math.h>

void kiir (double tomb[], int db)
{
	int i;
	for (i=0; i<db; i++)
	printf("PageRank [%d]: %lf\n", i, tomb[i]);
}

double tavolsag(double pagerank[], double pagerank_temp[], int db)
{
	double tav = 0.0;
	int i;
	for(i=0; i<db; i++) {
		//tav += abs(pagerank[i] - pagerank_temp[i]);
		tav += (pagerank_temp[i] - pagerank[i]) * (pagerank_temp[i] - pagerank[i]);
	}
	return sqrt(tav);
}

int main(void)
{
	double L[4][4] = {
	{0.0, 0.0, 1.0 / 3.0, 0.0},
	{1.0, 1.0 / 2.0, 1.0 / 3.0, 1.0},
	{0.0, 1.0 / 2.0, 0.0, 0.0},
	{0.0, 0.0, 1.0 / 3.0, 0.0}
	};

	double PR[4] = {0.0, 0.0, 0.0, 0.0};
	double PRv[4] = {1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0};

	int i,j;
	//i=0; j=0; h=5;

	for (;;) {

		for(i=0; i<4; i++) {
			PR[i] = 0.0;
			for (j = 0; j<4; j++)
				PR[i] += (L[i][j] * PRv[j]);
		}

		if (tavolsag(PR,PRv, 4) < 0.00000001) 
			break;
		
		for (i=0; i<4; i++)
			PRv[i] = PR[i];
	}

	kiir (PR,4);

	return 0;
}
} ]]></programlisting>   
        <para>
            Az internetes oldalak rangsorolása manapság népszerű. A Page-Rank érték azt hivatott megadni, hogy adott oldalak közül melyik a népszerűbb oly módon, hogy mennyi, és 'milyen erős' hivatkozás mutat rájuk. Így működik többek között a Google kereső is, bár nem ez az egyetlen algoritmus, amit követnek. Nagyon jó példa jelenik meg a probléma reprezentálására a https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog1_2.pdf linken található diasor 58. oldalán. Elénk vetül ugyanis egy gráfszerű ábrázolással előállított példa, és a hozzá tartozú linkmátrix. Az ábráról leolvasható, hogy a JP-re megy a legtöbb hivatkozás, így arra várjuk a legnagyobb értéket.
        </para>
		<para>
            Az itt megjelenő linkmátrix alapján írodott meg a fent leírt forráskód is, melyre rendre a következő értékek jöttek ki: 
        </para>
		<para>
			PageRank [0]: 0.090909	(J)
		</para>
		<para>
			PageRank [1]: 0.545455	(JP)
		</para>
		<para>
			PageRank [2]: 0.272727	(JPL)
		</para>
		<para>
			PageRank [3]: 0.090909	(M)
		</para>
		<para>
			Tehát a vártnak megfelelően alakult az eredmény. Sokkal izgalmasabb egy ilyen vizsgálat több oldal bevonása esetén. Korábban, a Facebook közösségi oldal megjelenése előtt a legjobb tudomásom szerint hölgyeket rangsoroló program is megjelent, ami hasonló elven működött.
		</para>
<para>
A program futtatása:
</para>

<figure>
<title>Az oldalakat rangsoroló programmodell futtatása</title>
<mediaobject>
<imageobject>
<imagedata fileref="img/turing_pagerank.png"/>
</imageobject>
<textobject>
<phrase>Az oldalakat rangsoroló programmodell futtatása</phrase>
</textobject>
</mediaobject>
</figure>


    </section>
                                                                                                                                                                                                                                                                                                                                                        
    <section xml:id="bhax-textbook-feladatok-turing.MontyHall">
        <title>A Monty Hall probléma</title>
        <para>
            Írj R szimulációt a Monty Hall problémára!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://bhaxor.blog.hu/2019/01/03/erdos_pal_mit_keresett_a_nagykonyvben_a_monty_hall-paradoxon_kapcsan">https://bhaxor.blog.hu/2019/01/03/erdos_pal_mit_keresett_a_nagykonyvben_a_monty_hall-paradoxon_kapcsan</link>
        </para>
        <para>
            Megoldás forrása: 
<link xlink:href="Turing/mh.r">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/mh.r</filename>
</link>
        </para>
        <para>
            Mielőtt az R szimulációt elvégeztem, megnéztem a Las Vegas ostroma című filmet, amelyben megjelenik a Monty Hall probléma. Először számomra nem volt világos, hogy miért lesz nagyobb esélyem nyerni, ha változtatok. Ha a három közül egy ajtó kinyílt, először az 50-50 %-os maradék-esélyre gondoltam. Átgondolva az esetet, mivel mindenképpen olyan ajtó nyílik ki először, ami mögött nincs nyeremény, a változtatással 66 %-ra növelem a nyerési esélyemet. Ez abból fakad, hogy ha az első ajtó-kiválasztásnál nem a nyereményre mutatok, csupán egyetlen ajtó marad az ajtónyitás következtében: a nyerő. Ellenkező esetben két ajtó közül választhat a játékvezető.
        </para>
		<para>
            A probléma bizonyítására végül az R programkód lefuttatása adott biztos magyarázatot. Eredetileg 10 millió próbajátékot végezne el a program, én 1 millió esettel dolgoztam, a számítógép-erőforrás korlátai miatt. A nyerő ajtó számát és az általam megadott ajtóét véletlenszám-generátorral adjuk meg minden esetben. Ezt követően a műsorvezető olyan ajtót választ, amelyik nem egyezik meg sem az általam választott ajtóéval, sem a nyerővel. Végül kiírja azon eredmények arányát, amikor a második ajtót a jelenlegi tipp megváltoztatásával, illetve változtatása nélkül nyitom ki. Az eredmény elegendően sok játszma esetén kiadja a 2:1 arányt az előbbi javára. A konklúzió: érdemes változtatni!
        </para>


<figure>
<title>A végeredmény megjelenik a konzolablakon</title>
<mediaobject>
<imageobject>
<imagedata fileref="img/turing_mh.png"/>
</imageobject>
<textobject>
<phrase>A végeredmény megjelenik a konzolablakon</phrase>
</textobject>
</mediaobject>
</figure>


    </section>

    <section xml:id="Brun">
        <title>100 éves a Brun tétel</title>
        <para>
            Írj R szimulációt a Brun tétel demonstrálására!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/xbYhp9G6VqQ">https://youtu.be/xbYhp9G6VqQ</link>
        </para>
        <para>
            Megoldás forrása: 
<link xlink:href="Turing/brun.r">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/brun.r</filename>
</link>
        </para>

<tip>
<para>Előfeltételek</para>
<para>Mielőtt a program futtatását megkíséreljük, telepíteni kell az R fejlesztői környezetet. Ez a Pythonhoz hasonló interpreteres nyelv, sőt, még inkább a Matlabhoz hasonlít. Ezen kívül, mivel matlab funkciókat is alkalmazunk a szkriptben, telepíteni kell a matlab modult a következő sor beírásával, az R interpreterben: install.packages("matlab")</para>
</tip>




        <para>
            A természetes számok építőelemei a prímszámok. Abban az értelemben, 
            hogy minden természetes szám előállítható prímszámok szorzataként.
            Például 12=2*2*3, vagy például 33=3*11.
        </para>
        <para>
            Prímszám az a természetes szám, amely csak önmagával és eggyel 
            osztható. Eukleidész görög matematikus már Krisztus előtt tudta, 
            hogy végtelen sok prímszám van, de ma sem tudja senki, hogy 
            végtelen sok ikerprím van-e. Két prím ikerprím, ha különbségük 2.
        </para>
        <para>
            Két egymást követő páratlan prím között a legkisebb távolság a 2, 
            a legnagyobb távolság viszont bármilyen nagy lehet! Ez utóbbit 
            könnyű bebizonyítani. Legyen n egy tetszőlegesen nagy szám. 
            Akkor szorozzuk össze n+1-ig a számokat, azaz számoljuk ki az 
            1*2*3*… *(n-1)*n*(n+1) szorzatot, aminek a neve (n+1) faktoriális, 
            jele (n+1)!.
        </para>
        <para>
            Majd vizsgáljuk meg az a sorozatot:
        </para>    
        <para>
            (n+1)!+2, (n+1)!+3,… , (n+1)!+n, (n+1)!+ (n+1) ez n db egymást követő azám, ezekre (a jól ismert
            bizonyítás szerint) rendre igaz, hogy            
        </para>    
        <itemizedlist>
            <listitem>
                <para>(n+1)!+2=1*2*3*… *(n-1)*n*(n+1)+2, azaz 2*valamennyi+2, 2 többszöröse, így ami osztható kettővel</para>
            </listitem>
            <listitem>
                <para>(n+1)!+3=1*2*3*… *(n-1)*n*(n+1)+3, azaz 3*valamennyi+3, ami osztható hárommal</para>
            </listitem>
            <listitem>
                <para>...</para>
            </listitem>
            <listitem>
                <para>(n+1)!+(n-1)=1*2*3*… *(n-1)*n*(n+1)+(n-1), azaz (n-1)*valamennyi+(n-1), ami osztható (n-1)-el</para>
            </listitem>
            <listitem>
                <para>(n+1)!+n=1*2*3*… *(n-1)*n*(n+1)+n, azaz n*valamennyi+n-, ami osztható n-el</para>
            </listitem>
            <listitem>
                <para>(n+1)!+(n+1)=1*2*3*… *(n-1)*n*(n+1)+(n-1), azaz (n+1)*valamennyi+(n+1), ami osztható (n+1)-el</para>
            </listitem>
        </itemizedlist>
        <para>
            tehát ebben a sorozatban egy prim nincs, akkor a (n+1)!+2-nél 
            kisebb első prim és a (n+1)!+ (n+1)-nél nagyobb első 
            prim között a távolság legalább n.            
        </para>    
        <para>
            Az ikerprímszám sejtés azzal foglalkozik, amikor a prímek közötti 
            távolság 2. Azt mondja, hogy az egymástól 2 távolságra lévő prímek
            végtelen sokan vannak.
        </para>    
        <para>
            A Brun tétel azt mondja, hogy az ikerprímszámok reciprokaiból képzett sor összege, azaz
            a (1/3+1/5)+ (1/5+1/7)+ (1/11+1/13)+... véges vagy végtelen sor konvergens, ami azt jelenti, hogy ezek
            a törtek összeadva egy határt adnak ki pontosan vagy azt át nem lépve növekednek, 
            ami határ számot B<subscript>2</subscript> Brun konstansnak neveznek. Tehát ez
            nem dönti el a több ezer éve nyitott kérdést, hogy az ikerprímszámok halmaza végtelen-e? 
            Hiszen ha véges sok van és ezek
            reciprokait összeadjuk, akkor ugyanúgy nem lépjük át a B<subscript>2</subscript> Brun konstans értékét, 
            mintha végtelen 
            sok lenne, de ezek már csak olyan csökkenő mértékben járulnának hozzá a végtelen sor összegéhez, 
            hogy így sem lépnék át a Brun konstans értékét.     
        </para>
        <para>
            Ebben a példában egy olyan programot készítettünk, amely közelíteni próbálja a Brun konstans értékét.
            A repó <link xlink:href="../../../bhax/attention_raising/Primek_R/stp.r">
                <filename>bhax/attention_raising/Primek_R/stp.r</filename>
            </link> mevű állománya kiszámolja az ikerprímeket, összegzi
            a reciprokaikat és vizualizálja a kapott részeredményt.
        </para>
        <programlisting language="R">
<![CDATA[#   Copyright (C) 2019  Dr. Norbert Bátfai, nbatfai@gmail.com
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

library(matlab)

stp <- function(x){

    primes = primes(x)
    diff = primes[2:length(primes)]-primes[1:length(primes)-1]
    idx = which(diff==2)
    t1primes = primes[idx]
    t2primes = primes[idx]+2
    rt1plust2 = 1/t1primes+1/t2primes
    return(sum(rt1plust2))
}

x=seq(13, 1000000, by=10000)
y=sapply(x, FUN = stp)
plot(x,y,type="b")
]]>
        </programlisting>        
        <para>
            Soronként értelemezzük ezt a programot:
        </para>                
        <programlisting language="R">
<![CDATA[ primes = primes(13)]]>
        </programlisting>        
        <para>
            Kiszámolja a megadott számig a prímeket.             
        </para>
        <screen>
<![CDATA[> primes=primes(13)
> primes
[1]  2  3  5  7 11 13
]]>
        </screen>
                
        <programlisting language="R">
<![CDATA[ diff = primes[2:length(primes)]-primes[1:length(primes)-1]]]>
        </programlisting>        
        <screen>
<![CDATA[> diff = primes[2:length(primes)]-primes[1:length(primes)-1]
> diff
[1] 1 2 2 4 2
]]>
        </screen>        
        <para>
            Az egymást követő prímek különbségét képzi, tehát 3-2, 5-3, 7-5, 11-7, 13-11.
        </para>
        <programlisting language="R">
<![CDATA[idx = which(diff==2)]]>
        </programlisting>        
        <screen>
<![CDATA[> idx = which(diff==2)
> idx
[1] 2 3 5
]]>
        </screen>              
        <para>
            Megnézi a <varname>diff</varname>-ben, hogy melyiknél lett kettő az eredmény, mert azok az ikerprím párok, ahol ez igaz.
            Ez a <varname>diff</varname>-ben lévő 3-2, 5-3, 7-5, 11-7, 13-11 külünbségek közül ez a 2., 3. és 5. indexűre teljesül.
        </para>
        <programlisting language="R">
<![CDATA[t1primes = primes[idx]]]>
        </programlisting>  
        <para>
            Kivette a primes-ból a párok első tagját. 
        </para>
        <programlisting language="R">
<![CDATA[t2primes = primes[idx]+2]]>
        </programlisting>        
        <para>
            A párok második tagját az első tagok kettő hozzáadásával képezzük.
        </para>
        <programlisting language="R">
<![CDATA[rt1plust2 = 1/t1primes+1/t2primes]]>
        </programlisting>        
        <para>
            Az 1/t1primes a t1primes 3,5,11 értékéből az alábbi reciprokokat képzi:
        </para>
        <screen>
<![CDATA[> 1/t1primes
[1] 0.33333333 0.20000000 0.09090909
]]>
        </screen>                      
        <para>
            Az 1/t2primes a t2primes 5,7,13 értékéből az alábbi reciprokokat képzi:
        </para>
        <screen>
<![CDATA[> 1/t2primes
[1] 0.20000000 0.14285714 0.07692308
]]>
        </screen>                      
        <para>
            Az 1/t1primes + 1/t2primes pedig ezeket a törteket rendre összeadja.
        </para>
        <screen>
<![CDATA[> 1/t1primes+1/t2primes
[1] 0.5333333 0.3428571 0.1678322
]]>
        </screen>                      
        <para>
            Nincs más dolgunk, mint ezeket a törteket összeadni a 
            <function>sum</function> függvénnyel.
        </para>
        
        <programlisting language="R">
<![CDATA[sum(rt1plust2)]]>
        </programlisting>    
        <screen>
<![CDATA[>   sum(rt1plust2)
[1] 1.044023
]]>
        </screen>            
        <para>
            A következő ábra azt mutatja, hogy a szumma értéke, hogyan nő, egy határértékhez tart, a 
            B<subscript>2</subscript> Brun konstanshoz. Ezt ezzel a csipettel rajzoltuk ki, ahol először a fenti 
            számítást 13-ig végezzük, majd 10013, majd 20013-ig, egészen 990013-ig, azaz közel 1 millióig.
            Vegyük észre, hogy az ábra első köre, a 13 értékhez tartozó 1.044023.
        </para>
        <programlisting language="R">
<![CDATA[x=seq(13, 1000000, by=10000)
y=sapply(x, FUN = stp)
plot(x,y,type="b")]]>
        </programlisting>          
        <figure>
            <title>A B<subscript>2</subscript> konstans közelítése</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/BrunKorok.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>A B<subscript>2</subscript> konstans közelítése</phrase>
                </textobject>
            </mediaobject>
        </figure>                             
        <tip>
            <title>Werkfilm</title>
            <itemizedlist>
                <listitem>
                    <para>
                        <link xlink:href="https://youtu.be/VkMFrgBhN1g">https://youtu.be/VkMFrgBhN1g</link>
                    </para>    
                </listitem>                
                <listitem>
                    <para>
                        <link xlink:href="https://youtu.be/aF4YK6mBwf4">https://youtu.be/aF4YK6mBwf4</link>
                    </para>    
                </listitem>                
            </itemizedlist>                
        </tip>
    </section>

<section>
        <title>Vörös Pipacs Pokol/csiga folytonos mozgási parancsokkal</title>
        <para>
            Megoldás videó: 
<link xlink:href="https://youtu.be/Sk_rdSOsSxQ">https://youtu.be/Sk_rdSOsSxQ</link>
        </para>
<para>
Megoldás forrása: 
<link xlink:href="Turing/folytonos.py">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/folytonos.py</filename>
</link>
</para>

        <para>
            Az első fejezethez tartozó Minecraft Malmo-s feladat a csiga alakban haladás fölfelé, folytonos mozgási parancsokkal. Nem is maga a feladat bizonyult bonyolultnak, hanem a Malmo installálása, beizzítása. Helyesebben egy előtelepített verziót töltöttem le, ám ehhez meglehetősen sok előtelepítés kellett. Java megfelelő verziója és a Python 3 installálása után a pip3 segítségével lehet a Python programnyelv számára csomagokat telepíteni. Ezt követően letöltöttem a Bátfai Norbert tanár úr által megosztott alapkódot, melyből kiindulva meg lehetett valósítani a csigamozgást. Itt fontos megjegyezni, hogy a forráskód egy .py kiterjesztésű állományból áll, illetve egy xml fájl írja le a pálya tulajdonságait, illetve az is ott kerül rögzítésre, hogy milyen mozgási parancsokat alkalmazhatunk. Jelen esetben a folytonos mozgási parancsokkal dolgozunk.
        </para>  

<para>
Az alapkód adott, amelynek egyik legfőbb része a Steve osztály, melynek a run metódusába lehet beleírni a mozgási parancsokat. A példányosítást és a példány run() hívását a végén kíséreljük meg:
</para>          
<para>
steve = Steve(agent_host)
</para>
<para>
steve.run(n)
</para>
<para>
Itt az n azért kerül bele a run paraméterlistájába, mert a csigamozgást ezen változó vezérli, melynek alapértelmezetten a 4 értéket adtam meg. 
</para>
<para>
Ezen kívül tulajdonképpen csak a Steve osztály run() metódusát módosítottam, a következő módon:
</para>

<programlisting language="Python"><![CDATA[

    def run(self,n):
        world_state = self.agent_host.getWorldState()
        # Loop until mission ends:
        while world_state.is_mission_running:
            print("--- nb4tf4i arena -----------------------------------\n")
            
            self.agent_host.sendCommand("move 1")
            time.sleep(n/4)
            if (n%4==0):
                self.agent_host.sendCommand("jump 1")
                time.sleep(.5)
                self.agent_host.sendCommand("jump 0")
                time.sleep(.1)
            if (n%1==0):
                self.agent_host.sendCommand("turn 1")
                time.sleep(.5)
                self.agent_host.sendCommand("turn 0")
                time.sleep(.1)
            n+=1
                
            
            world_state = self.agent_host.getWorldState()

]]>
</programlisting>

<para>
A különböző mozgási parancsok a MissionHandlers.html állományban megtalálhatók a Schemas könyvtárban.
</para>
<para>
Kissé furcsa a folytonos mozgási parancs, ugyanis a ("move 1") parancs elindítja az ágenst teljes sebességgel, és amíg nem nem adjuk ki a ("move 0")-t, addig folyamatosan megy. Ugyanígy van ez a forgásnál is és egyéb mozgásoknál. 
</para>
<para>
Az n értéke kezdetben 4, ennyit halad előre először, mielőtt fordulni kellene. Majd a while ciklus minden egyes lefutásánál növelem az n értékét 1-el, előre pedig az n értéke/4 másodperc ideig várakozik Steve. Így egyre több ideig várakozunk, ami jogos, hisz minden szinten többet és többet kell haladni egyes fordulások- és ugrások között. Az ugrások és fordulások ütemezését a labdapattogásos feladathoz hasonlóan maradékos osztással oldottam meg, fordulni minden körben, ezzel szemben ugrani minden 4. körben kell. 
</para>

    </section>



</chapter>                
