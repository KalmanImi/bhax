#include <stdio.h>
#include <unistd.h>
#include <string.h>

#define MAX_KULCS 100
#define BUFFER_MERET 256

int
main (int argc, char **argv)
{

    char kulcs[MAX_KULCS];    //Létrehozunk egy tömböt a kulcsnak
    char buffer[BUFFER_MERET];    //Megadjuk a beolvasáshoz szükséges tároló mretét

    int kulcs_index = 0;  //A kulcs elemein történő lépegetéshez szükségeees bevezetni
    int olvasott_bajtok = 0;      //Később megadja, mennyi bájtot olvastunk be.

    int kulcs_meret = strlen (argv[1]); 
    //A kulcs méretét az első argumentum méretéhez igazítjuk.  
    strncpy (kulcs, argv[1], MAX_KULCS);
    //Itt a kulcs karakterekből álló tömb megkapja az első argumentum karaktersorozatát. 

    //printf(kulcs);
    //printf('\n');



    //Ebben a szakaszban történik a szöveg beolvasása, és a kizáró vagy művelet által 
    //átalakított szöveg kiíratása. A beolvasás közvetlenül a standard inputon keresztül
    //történik. Ezt jelenti a 0 szám, a read függvény első paramétereként. A második
    //paraméterbe 
    // illesztjük bele a beolvasott szöveget. Itt van egy void*, ami már korábban is előfordult
    //a háromszögmátrix létrehozásánál, típusényszerítést tudunk végrehajtani rajta. A
    //BUFFER_MERET pedig maximalizálja a beolvasott bájtok számát. Szóval a bufferrel dolgozunk
    //tovább. 
    //Az olvasott_bajtok változó számolja az eddig beolvasott bájtokat. 
    while ((olvasott_bajtok = read (0, (void *) buffer, BUFFER_MERET)))
    {
    //Mik a beolvasott értékek??? 

        for (int i = 0; i < olvasott_bajtok; ++i)
        //Végigmegyünk a standard inputon keresztül átadott bájtsorozaton
	    {

	        buffer[i] = buffer[i] ^ kulcs[kulcs_index];
            //A buffer bájtsorozat, melynek minden elemén elvégezzük a kizáró vagy műveletet.
            //Az érték, amit kapunk, az aktuális bájt és a kulcsban megadott érték függvénye lesz
            //oly módon, hogy 
	        kulcs_index = (kulcs_index + 1) % kulcs_meret;
            //A kulcs index egyesével lépeget fölfelé egészen addig, amíg el nem éri a kulcs 
            //hosszát. Utána mindig a 0-s indexre ugrik. A maradékos osztásnak köszönhetően 
            //tudunk mindig az elejére ugrani, így nem lépünk sosem túl a kulcs tömb elemein. 
            

	    }

        write (1, buffer, olvasott_bajtok);
        //Itt pedig kiíratjuk a xor művelettel kapott eredményt.

    }


    

    return 0;


}
