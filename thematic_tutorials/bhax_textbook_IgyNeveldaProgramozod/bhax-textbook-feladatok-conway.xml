<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Conway!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>

<para>
A fejezet első feladata egy hangyaszimuláció létrehozását célozza meg. Először kerül megemlítésre a reverse engineering kifejezés, így erről szeretnék pár gondolatot leírni. Korábban ugyanis a szoftverfejlesztés különböző, egymást követő fázisokból állt. Először a feladat elemzése, majd a tervezési folyamatok zajlottak, mely során UML diagrammokat készítettek. Így tervezték meg az osztályok közötti viszonyokat, hierarchiát. Ezt követően kezdődhetett el a programok írása, majd lezárásként a program tesztelése, esetleges frissítése. Napjainkban több érv is amellett szól, hogy ez a tervezési folyamat már a múltté. A legnyomósabb érv az időhiány. Emiatt megteremtették a lehetőséget a reverse engineering jellegű programírásra, amikor is először megírunk egy kódrészletet, és azon forrás alapján van lehetőségünk UML osztálydiagram elkészítésére. Így egyes részfolyamatoknál ellenőrizni tudjuk magunkat, ráadásul a legenerált diagrammon pontosan úgy jelennek meg az osztályok és kapcsolataik, ahogyan azt beprogramoztuk. Így a tervezett és a ténylegesen létrehozott állományok közötti különbség adta hibalehetőség is megszűnik. Nem beszélve arról, mennyi időt meg lehet így spórolni. 
</para>
<para>
A fejezet második és harmadik feladata a maga idejében nagy port felkavaró életjáték program megírása, helyesebben a programban egy olyan kezdőfeltétel megadása, ami által lépésről lépésre új sejtek születnek. Mint azt a példákban látjuk, egy siklóágyút kell létrehozni, ami az új sejtek generálását megvalósítja. Magának a programnak a részletes leírása a QT-C++ megvalósításhoz került. 
</para>
<para>
És végezetül a BrainB Benchmark programról írok dióhéjban. Ez egy olyan projekt eredetileg, amelyben két tudományterület: az informatika és a pszichológia ötvöződik. Tulajdonképpen arról van szó, hogy van egy adott objektum, és 10 percen keresztül azon objektum kijelölt részén kell tartani a kurzort úgy, hogy közben a pozíciója folyamatosan változik. További nehezítésként, minél tovább rajta tartjuk a kurzort, annál gyorsabban, nagyobb fluktuációval mozog az objektum, illetve további objektumok is megjelennek és gyakran az átfedések okoznak kellemetlen pillanatokat a koncentráló játékosnak. Ha letérünk a kijelölt részről, a háttérobjektumok száma csökken, a mozgás lelassul. A végén kapunk egy kiértékelést.
</para>
<para>
A fejezet rövid ismertetése után lássuk a konkrét programokat. 
</para>



    <section>
        <title>Hangyaszimulációk</title>
        <para>
            Írj Qt C++-ban egy hangyaszimulációs programot, a forrásaidról utólag reverse engineering jelleggel
            készíts UML osztálydiagramot is!
        </para>
<caution>
<title>Tutorom: Katona Andrea</title>
<para></para>
</caution>
        <para>
            Megoldás videó: <link xlink:href="https://bhaxor.blog.hu/2018/10/10/myrmecologist">https://bhaxor.blog.hu/2018/10/10/myrmecologist</link>
        </para>
        <para>
            Megoldás forrása: 
<link xlink:href="Conway/hangya/ant.h">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/hangya/ant.h</filename>
</link>, 
<link xlink:href="Conway/hangya/antthread.cpp">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/hangya/antthread.cpp</filename>
</link>, 
<link xlink:href="Conway/hangya/antthread.h">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/hangya/antthread.h</filename>
</link>, 
<link xlink:href="Conway/hangya/antwin.h">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/hangya/antwin.h</filename>
</link>, 
<link xlink:href="Conway/hangya/antwin.cpp">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/hangya/antwin.cpp</filename>
</link>, 
<link xlink:href="Conway/hangya/main.cpp">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/hangya/main.cpp</filename>
</link>
        </para>


<para>
A hangyaszimuláció az előzőkhöz képest már egy bonyolultabb QT-s projekt létrehozását követeli meg. A programot 6 különböző egységre bontjuk, az egyszerűség kedvéért. Ezeket egyenként szeretném bemutatni. Mivel a könyv terjedelmét jelentősen növelné, az átláthatóságot pedig csökkentené, a forráskódot megjelenítés helyett linkelem, és a benne szereplő információkról írok egyenként. 
</para>

<itemizedlist>
<listitem>
<para> ant.h </para>
<para>
A hangya objektumat innen lehet példányosítani. Ez ugyanis egy osztály, amely a később létrehozott objektumoknak tartalmazni fogja a pozícióját a két dimenziós síkban, és az irányát. Csupán egy konstruktorral bír, nincsenek tagfüggvényei.
</para>
</listitem>


<listitem>
<para> antthread.h </para>
<para>
Az AntThread egy, az előzőnél sokkal bonyolultabb osztály. Konstruktorában megkapja a main-ben argumentumként beolvasott értékeket (vagy az alapértelmezett értékeket használja), gyakorlatilag mindent, ami a program futásához szükséges. Létezik továbbá egy destruktora is az osztálynak. A <function>run()</function>, <function>finish()</function>, <function>pause()</function>, <function>isRunning()</function> eljárások és függvények az antthread.cpp-ben vannak kifejtve, a program futásával kapcsolatos információk, lehetőségek tartoznak ide. 
</para>
<para>
Térjünk át a más osztályok számára láthatatlan private kulcsszóval ellátott változókra, eljárásokra. Tulajdonképpen ide érkeznek a konstruktorban megadott tagok. Ezen felül a hangyák mozgását befolyásoló tényezők változását megvalósító eljárások foglalnak helyet itt, azonkak a vázát írjuk le. Most következik ezen eljárásoknak a részletezése, amelyet az antthread.cpp állomány ír le.
</para>
</listitem>


<listitem>
<para> antthread.cpp </para>
<para>
Elsőként a konstruktor kifejtése történik, ami annyit csinál, hogy a bevezetett privát válzotóknak átadja a paraméterlistában szereplő értékeket. Továbbá itt hozzuk létre a hangya objektumokat, numAnts db-ot, melyek kezdőpozícióját egy már korábban alkalmazott randomszám generátor segítségével írjuk le. Egy egy ilyen objektum elkészültekor azokat hozzáfűzzük az ants listához. 
</para>
<para>
A <function>sumNbhs</function> függvényben először lefuttatjuk a <function>detDirs()</function> függvényt, azat lekérjük az irányultságot, beállítva az ifrom, ito, jfro, jto értékét. Majd a dupla for ciklusban megadjuk az új pozíciót, azokat az eseteket is figyelembe véve, amikor a hangyák 'lemásznának' a tábláról. Végül visszatérünk a sum értékkel, melyet szintén itt számolunk ki, a pozíció függvényében......
</para>

</listitem>

<listitem>
<para>antwin.h</para>
<para>
Az AntWin osztály az ablak kialakításáért és a rajzolásért felelős, mintegy keretbe foglalja a programot. Konstruktora paraméterként kapja meg a méreteket, a képkockák közötti időintervallumot, a hangyák számát, a feromonra jellemző értékeket és egyéb értékeket. Megadunk továbbá néhány eseményt is (keyPressEvent), melyeket adott billenytűk lenyomása során érünk el. Ilyen például a megállítás a 'P' billenytű lenyomására, illetve a kilépés a 'Q' billenytű lenyomására. Az osztálynak szintén privátok a tagjai. Az itt definiált függvények viszont publikusak. Ezek kzül nagy jelentőségű a fentieken kívül a paintEvent. 
</para>
</listitem>

<listitem>
<para>antwin.cpp</para>
<para>
Szokás szerint a konstruktor kifejtésével kezdjük. Miután átadjuk a megfelelő értékeket az AntWin osztálynak, a cellákkal foglalkozunk. Ugyanis az ablakon egy rácsozás fog megjelenni, a hangyák pedig ezen rácsokban tudnak mozogni. Itt beállítjuk, hogy az egyes rácsok magassága- és szélessége egyaránt 6 pixel legyen. Azt is beállítjuk továbbá, hogy az ablak fix méretű legyen, a cellák számával arányos méretű. Miután az alap elkészült, jöhetnek rá a hangyák. Ehhez példányosítunk hangya objektumokat (ants), elhelyezzük majd elindítjuk őket.
</para>
<para>
A paintEvent a színezésért felelős. Külön rajzoljuk fel az alaptáblát, a hangyákat és az általuk húzott feromoncsíkokat. Ez utóbbiakat a zöld szín különböző árnyalataival tesszük. 
</para>
<para>
És végül a destruktor jelenik meg, ami a létrehozott objektum törléséért felelős. A step függvény pedig a képfrissítést végzi az <function>update()</function> hívásával.
</para>
</listitem>


<listitem>
<para>main.cpp</para>
<para>
Végezetül a main.cpp kerül jellemzésre, ami mondhatni, az indító szikra. Ide be kell inkludálnunk néhány QT headert a működéshez, illetve az antwin.h headert. A main metódusban rögtön egy elegáns QT-s megoldás szerepel. A QT saját argumentumkezelőjét használjuk, amely során megadunk alapértékeket, de lehetőségünk van a program indításakor is megadni a kezdő értékeket. Miután a megadott paramétereket lekezeltük, létrehozzuk az AntWin osztály egy példányát, átadjuk neki a paramétereket, és meghívjuk a példány show() függvényét. A paraméterátadásnál fontos, hogy integer típusra váltjuk az eredeti karakterláncot.
</para>
</listitem>
</itemizedlist>

<para>
Nem maradt más hátra, mint a fordítás és a futtatás. A fordítást megelőzve egy új QT projekt létrehozásával kezdünk. Majd ezt követően a qmake és make parancsok segítségével fordítjuk a forrást, majd futtatjuk a megszokott módon. Fontos leellenőrizni a fordítást megelőzően, hogy szerepel-e minden forrásfájl a projektben. 
</para>
<para>
A programot futtatva a következő ablak jelenik meg a képernyőn:
</para>

<figure>
<title>A hangyaszimuláció működés közben</title>
<mediaobject>
<imageobject>
<imagedata fileref="img/Conway_hangya.png"/>
</imageobject>
<textobject>
<phrase>A hangyaszimuláció működés közben</phrase>
</textobject>
</mediaobject>
</figure>

<para>
A program megfelelően működik, a P lenyomására szüneteltethető a hangyák mozgása, a Q lenyomására pedig kilépünk a programból. 
</para>        
    </section>  






      
    <section>
        <title>Java életjáték</title>
        <para>
            Írd meg Java-ban a John Horton Conway-féle életjátékot, 
            valósítsa meg a sikló-kilövőt!
        </para>
        <para>
            Megoldás videó: 
        </para>
        <para>
            Megoldás forrása: 
<link xlink:href="Conway/eletjatek_java/Sejtautomata.java">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/eletjatek_java/Sejtautomata.java</filename>
</link>
        </para>

<para>
John Horton Conway életjáték programjáról a C++-os megvalósításnál írok részletesebben. Elöljáróban annyit megemlítek, hogy van egy ablak, mely rácsozott (az előző példához hasonlóan), és lesznek olyan rácspontok, amelyeken élő sejtek helyezkednek el (fekete), és olyanok, amelyek üresek. Van egy szabály arra vonatkozólag, hogy hogyan születhetnek, hogyan maradhatnak életben, illetve hogyan halnak meg sejtek. Ennek részletezése a programkód elemzése során- és a C++-os megvalósításnál lesz. Először nézzük meg a Java-s változatot. 
</para>
<programlisting language="Java"><![CDATA[
public class Sejtautomata extends java.awt.Frame implements Runnable {
    /** Egy sejt lehet élő */
    public static final boolean ÉLŐ = true;
    /** vagy halott */
    public static final boolean HALOTT = false;
    /** Két rácsot használunk majd, az egyik a sejttér állapotát
     * a t_n, a másik a t_n+1 időpillanatban jellemzi. */
    protected boolean [][][] rácsok = new boolean [2][][];
    /** Valamelyik rácsra mutat, technikai jellegű, hogy ne kelljen a
     * [2][][]-ból az első dimenziót használni, mert vagy az egyikre
     * állítjuk, vagy a másikra. */
    protected boolean [][] rács;
    /** Megmutatja melyik rács az aktuális: [rácsIndex][][] */
    protected int rácsIndex = 0;
    /** Pixelben egy cella adatai. */
    protected int cellaSzélesség = 20;
    protected int cellaMagasság = 20;
    /** A sejttér nagysága, azaz hányszor hány cella van? */
    protected int szélesség = 20;
    protected int magasság = 10;
    /** A sejttér két egymást követő t_n és t_n+1 diszkrét időpillanata
     közötti valós idő. */  
    protected int várakozás = 1000;
    // Pillanatfelvétel készítéséhez
    private java.awt.Robot robot;
    /** Készítsünk pillanatfelvételt? */
    private boolean pillanatfelvétel = false;
    /** A pillanatfelvételek számozásához. */
    private static int pillanatfelvételSzámláló = 0;
]]></programlisting>
<para>
Rögtön az elején, ahogyan azt a Java nyelv esetében megszokhattuk, egy osztály létrehozásával kezdünk. A Sejtautomata osztály örökli a java.awt.Frame tulajdonságait, ami lehetőséget ad az ablakkezelésre, színezésre. Hasonló ez, mint a C++ esetén a QT. Java esetén alapértelmezetten a beépített awt és Swing osztályok kerülnek előtérbe, ha GUI interfészt szeretnénk létrehozni. Ezt követően az osztály tagváltozóit vezetjük be, miközben megadjuk a láthatóságra vonatkozó kulcsszót. Itt minden változó esetén külön-külön adjuk meg, alapértelmezetten a C+-ban megszokott módon a változó privát lesz, azaz más objektum számára nem hozzáférhető. 
</para>
<para>
Az előző projekthez hasonlóan az ablakméretek, a rácsméretek jelennek meg a tagváltozók között, továbbá a cellára vonatkozó 'sejt információ' (van-e ott élő sejt), valamint képernyőfelvétel készítéséhez szükséges változók.
</para>

<para>
A következő sorok már a konstruktort és az osztály további függvényeit, eljárásait tartalmazzák. Mivel a mellékletben megadott forrásfájl részletesen kommentezett, külön nem térek ki minden részre, csak nagyvonalakban írok az eseményekről, tapasztalatokról. Az alapfelület létrehozásakor minden egyes cellaértéket halottra állítunk, miután az ablakot létrehoztuk. Ezt követően, a sikló és a siklóKilövő eljárásokban állítjuk be azon rácspontokat élőre, amelyek kirajzolják a siklóágyút. Mindezt a kezdetekkor adjuk meg, a képfrissítések előtt. Ha a kezdeti feltételeket megadva elindítjuk a folyamatot, láthatóvá válik, hogy a siklóágyú elkezd sejttömböket kilőni magából, azaz egyfajta sejtgenerátorként működik. 
</para>
<para>
Arra, hogy egy adott részen mi fog történni (születés/halálozás/életben maradás), szükség van a szomszédok számára. Ha ez az érték túl kevés, elszigetelődés miatt meghal az egyedsejt, ha túl nagy, kiszelektálódik. Optimális esetben (2 vagy 3) a sejt megmarad vagy új születik.
</para>
<para>
A sejttér kirajzolása, és a program futása közbeni interaktivitás megtartása az awt-nek köszönhető. Lehetőségünk van - többek között - új sejtek táblára rajzolására futás közben. Ezért a mousePressed felelős. A pillanatfelvétel készítése az S lenyomásával lehetséges. A G lenyomásával gyorsítjuk, az L lenyomásával lassítjuk az animációt.
</para>
<para>
A program fordítását a javac Sejtautomata.java paranccsal hajtjuk végre, majd futtatjuk: java Sejtautomata.
</para>

<figure>
<title>A program futása</title>
<mediaobject>
<imageobject>
<imagedata fileref="img/Conway_java_eletjatek.png"/>
</imageobject>
<textobject>
<phrase>A program futása</phrase>
</textobject>
</mediaobject>
</figure>

<figure>
<title>Reagálás a kattintásra</title>
<mediaobject>
<imageobject>
<imagedata fileref="img/Conway_java_eletjatek-katt.png"/>
</imageobject>
<textobject>
<phrase>Reagálás a kattintásra</phrase>
</textobject>
</mediaobject>
</figure>        

            
    </section>  



      
    <section>
        <title>Qt C++ életjáték</title>
        <para>
            Most Qt C++-ban!
        </para>
        <para>
            Megoldás videó: 
        </para>
        <para>
            Megoldás forrása: 
<link xlink:href="Conway/eletjatek_qt/sejtablak.h">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/eletjatek_qt/sejtablak.h</filename>
</link>,
<link xlink:href="Conway/eletjatek_qt/sejtablak.cpp">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/eletjatek_qt/sejtablak.cpp</filename>
</link>,
<link xlink:href="Conway/eletjatek_qt/sejtszal.h">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/eletjatek_qt/sejtszal.h</filename>
</link>,
<link xlink:href="Conway/eletjatek_qt/sejtszal.cpp">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/eletjatek_qt/sejtszal.cpp</filename>
</link>,
<link xlink:href="Conway/eletjatek_qt/main.cpp">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/eletjatek_qt/main.cpp</filename>
</link>,
        </para>



<para>
John Horton Conway életjáték programja a maga idejében igen komoly anyagi károkat okozott a számítógépeket üzemeltető bankoknak- és biztosítótársaságoknak, elvesztegetett gépidő formájában. A programozók között ugyanis gyorsan elterjedt ezen egyszemélyes játék, melynek megoldása izgalmakat rejtett. A kitláló matematikus úgy írta meg a programot, hogy egy adott kezdőpozíciót megadva kvázi kiszámíthatatlan lesz a kimenetel. Ezt három egyszerű szabállyal érte el, egy rácsozott felületen:
</para>
<para>
Túlélés: azon sejtek, melyeknek kettő vagy három szomszédja van, életben marad.
</para>
<para>
Halál: azon sejtek, melyeknek 4- vagy több szomszédja va, a túlnépesedéstől meghalnak, továbbá amelynek kevesebb mint két 'egy szomszédja' van, elszigetelődés miatti elhalálozás következik be.
</para>
<para>
Születés: ha egy üres cellának pontosan 3 élő sejt van a szomszédjában, ott ott új sejt születik.
</para>
<para>
Ezen szabályok által változik az élő cellák populációja nemzedékről nemzedékre. A Conway által kitűzött feladat, melynek megoldására 50 dollár jutalmat tűzött ki, olyan alakzatnak a létrehozása, mely korlátlanul növekszik. Itt jön képbe a siklóágyú, amely ezt képes megvalósítani, és amely alakzatot létrehozzuk, megjelenítjük Qt ablakban. 
</para>

<para>
Az életjáték program 5 fő állományból áll. 2 header állományt hozunk létre, melyek egy-egy osztályt reprezentálnak. Ezekhez tartozik egy-egy cpp állomány, ahol az osztályok különböző tagfüggvényeit definiáljuk. Végül egy main.cpp állomány adott, amelyben a program elindítása valósul meg. Nézzük ezen állományokat részletesebben.
</para>

<itemizedlist>
<listitem>
<para> sejtablak.h </para>

<para>
A SejtAblak osztály konstruktora megengedi, hogy tetszőleges méretű ablakot jelenítsünk meg, ezen paraméterek megadásának hányában egy 100x75-ös rácsot kapunk. Az osztálynak létezik egy destruktora is. Idegen objektumok számára látható a vissza egyetlen paraméterrel rendelkező eljárás, melynek részletezése a sejtablak.cpp állományban található. Hasonló a helyzet a paintEvent, siklo és a sikloKilovo eljárásokkal, ám ezek protected kulcsszóval ellátottak, tehát 'védettek' más objektumokkal szemben. Ugyanígy az alaptáblára vonatkozó értékek is védettek, azaz friend viszony ellenében nem módosíthatja őket más objektum. Van még egy utolsó objektum, melyet példányosítunk és private kulcsszóval védett, ez egy <type>SejtSzal</type> típusú objektumra mutató mutató. 
</para>

</listitem>


<listitem>
<para> sejtablak.cpp </para>

<para>
A sejtablak.cpp állományban a SejtAblak osztály konstruktorának kifejtésével foglalkozunk először. Beállítjuk az ablak címét, megadjuk annak méreteit, és rácsozást vezetünk be a megadott paraméterek alapján. A rácsokban fogjuk ábrázolni a sejteket. Ha élő sejt helyezkedik el egy rácspontban, azt feketére fogjuk színezni, egyébként fehéren hagyjuk. A színezést azonban majd a <function>paintEvent</function> fogja elvégezni, itt csak a színezés alapját képező alapadatok kerülnek rögzítésre. A for ciklusokkal bejárunk minden egyes rácspontot, és alapértelmezetten halottra állítjuk azokat. Ezt követően meghívjuk a <function>sikloKilovo</function> eljárást, amely élő sejteket képez, megrajzolva a siklókilövőt. Ehhez tudnunk kell, mely rácsokon kell élő sejteknek lenni ahhoz, hogy mindig új sejteket generáljon. Ez által a racs[y][x] hivatkozással az adott rácspontokat élőre tudjuk állítani.
</para>
<para>
Szintén a konstruktor törzsében végrehajtódik egy SejtSzal példányosítás is, az objektum neve eletjatek. Létrehozását követően annak start függvényét hívjuk meg. Ezzel a lendülettel szeretnék is áttérni a sejtszal.h és a sejtszal.cpp állományokra. 
</para>
</listitem>


<listitem>
<para>sejtszal.h</para>
<para>
A SejtSzal osztály szerkezete a sejtszal.h fájlban lett eltárolva. Itt fogjuk kiszámolni, hogy az elkövetkező időpillanatban milyen változás következik be a sejtpopulációban. Mely sejtek halnak meg elszigetelődés- vagy túlzsúfoltság következtében, illetve mely sejtek maradnak életben, és hol következik be születés. Ehhez szükség van a korábban létrehozott ablakra és annak adataira, a rácsra és a rácspontokról szóló információkra. Mindezeket a konstruktoron keresztül adjuk át a <type>Sejtszal</type> típusú <varname>eletjatek</varname> objektumnak.
</para>
<para>
Az osztálynak a konstruktoron kívül egy publikus eljárása, a <function>run()</function> létezik, illetve a destruktora. protected kulcsszöval védett minden példányváltozója és további eljárásai, függvényei. Ilyen függvény a szomszedokSzama, amely egészként visszatér a szomszédos sejtek számával, melynek maximuma 8 lehet, és amely a sejtek életben maradásáért felelősek. Fontos még az idoFejlodes eljárás is, amelynek részletezése a cpp állományban szerepel.
</para>
</listitem>


<listitem>
<para>sejtszal.cpp</para>
<para>
Először a konstruktor kerül kifejtésre. Egyszerűen a SejtAblak megfelelő adatait átadjuk jelen példány megfelelő értékeinek. A <function>szomszedokSzama</function> kiszámolja az adott rácspont körüli szomszédok számát. Mindezt egy 3x3-as rács értékeiből számolja ki, úgy, hogy az aktuális pont van középen, és azt nem vesszük figyelembe. Ezzel a számmal térünk vissza, és az <function>idoFejlodes()</function> eljárás ez alapján számolja ki, hogy mely sejtek születnek vagy éppen halnak meg, és melyek maradnak élve. A <function>run()</function> ezen vizsgálat folytonosságát biztosítja, azaz egy animációt készítünk. Márcsak a main.cpp maradt hátra.
</para>
</listitem>


<listitem>
<para>main.cpp</para>
<para>
Beinkludáljuk a megfelelő fejállományokat, ablakképzéshez szükséges állományokat. Majd a main metódusban példányosítjuk a SejtAblakot, és hívjuk annak show() függvényét. Ez által az animáció elindul.
</para>
</listitem>
</itemizedlist>
<para>
Nincs más hátra, mint a program fordítása és futtatása. Ehhez az 5 db forrásfájl együttes jelenléte szükséges, kiadjuk a qmake majd a make parancsot. Végül a ./eletjatek által indul el a program.
</para>

<figure>
<title>Fordítás</title>
<mediaobject>
<imageobject>
<imagedata fileref="img/Conway_eletjatek1"/>
</imageobject>
<textobject>
<phrase>Fordítás</phrase>
</textobject>
</mediaobject>
</figure>

<figure>
<title>Futtatás, az ágyú beindul</title>
<mediaobject>
<imageobject>
<imagedata fileref="img/Conway_eletjatek2"/>
</imageobject>
<textobject>
<phrase>Futtatás, az ágyú beindul</phrase>
</textobject>
</mediaobject>
</figure>

<figure>
<title>Sejtek sorozatgyártása</title>
<mediaobject>
<imageobject>
<imagedata fileref="img/Conway_eletjatek3"/>
</imageobject>
<textobject>
<phrase>Sejtek sorozatgyártása</phrase>
</textobject>
</mediaobject>
</figure>

        


          
    </section>        
    <section>
        <title>BrainB Benchmark</title>
        <para>
        </para>
        <para>
            Megoldás videó: 
        </para>
        <para>
            Megoldás forrása: 
<link xlink:href="Conway/BrainB/BrainBThread.h">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/BrainB/BrainBThread.h</filename>
</link>, 
<link xlink:href="Conway/BrainB/BrainBThread.cpp">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/BrainB/BrainBThread.cpp</filename>
</link>, 
<link xlink:href="Conway/BrainB/BrainBWin.h">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/BrainB/BrainBWin.h</filename>
</link>
<link xlink:href="Conway/BrainB/BrainBWin.cpp">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/BrainB/BrainBWin.cpp</filename>
</link>
<link xlink:href="Conway/BrainB/main.cpp">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/BrainB/main.cpp</filename>
</link>
      
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para> 
<para>
A BrainB Benchmark alapvetően a karakterelvesztésen illetve annak megtalálásán alapszik. Helyesebben, ha elveszítjük a képernyőn megjelenő karaktert, azt mérjük, mennyi ideig tart megtalálni, illetve ha megtaláltuk a karaktert, mennyi ideig tart elveszíteni. Természetesen az az optimális, ha a lehető legtöbb ideig sikerül rajta tartani az objektumon a kurzort. További fontos szerepe van az idő faktornak, ugyanis az is kikötés, hogy a 10 perces mérés idején végig lenyomva kell tartani a kurzort. 
</para>
<para>
A program, hasonlóan a csokor további tagjaihoz, több állományból áll és a megjelenítést szintén a QT segítségével végezzük. A program szerkezetileg is jelentős hasonlóságot mutat az előzőkhöz képest.
</para>
<para>
A main metódusba inkludáljuk be a BrainWin.h headert, és példányosítjuk. A BrainWin.h az ablak osztályt foglalja magában, legalábbis annak szerkezetét írja le. A BrainWin.cpp ennek a kifejtése, avagy a konstruktor és a függvények konkretizálása. 
</para>
<para>
Létezik továbbá a BrainThread.h és a BrainThread.cpp páros, amelyek a számítások elvégzéséért felelősek, ezek alkotják a program motorját. Avagy a headerben a megszokott módon létrehozzuk az objektumok vázát, a cpp-ben kifejtünk. Bár jelen esetben a headerben is több taggfüggvényt konkretizálunk. Az itt definiált osztály egyébként a Hero, ami az ablakon megjelenő objektumpéldányoknak a szerkezetét írja le. Itt nagyon fontos pontként jelenik meg egy vektornak a bevezetése, melynek mérete dinamikusan változtatható. Ennek azért van kulcsszerepe, mert a képernyőn a teljesítmény függvényében megjelenő objektumok ebben kerülnek letárolásra. Mivel az objektumok száma folyamatosan változik, indokolt a dinamikus listaszerű adatszerkezet a statikus tömbszerű helyett. 
</para>
<para>
A program fordítása és futtatása ugyanúgy a qmake és a make parancsokkal hajtható végre. Itt azonban fontos kiegészítés, hogy a qmake project létrehozását követően a project fájlokban benne kell lenni a QT+=widgets sornak, máskülönben nem tudunk egy, az ablakkezelésért felelős QT headert beinkludálni és hibát kapunk. Ezt követően qmake és make, és legenerálódik a futtatható állomány. 
</para>
<para>
A sikeres fordítás- és futtatás örömére a programot ki is próbáltam. A 10 perces teszt végén készült képernyőképről, és a szöveges fájlról, melyeket a program futtatása során megkapunk, képeket is mellékeltem:
</para>

<figure>
<title>10 perc koncentrálás eredménye</title>
<mediaobject>
<imageobject>
<imagedata fileref="img/Conway_BrainB.png"/>
</imageobject>
<textobject>
<phrase>10 perc koncentrálás eredménye</phrase>
</textobject>
</mediaobject>
</figure>

<figure>
<title>Ugyanez szöveges formátumban</title>
<mediaobject>
<imageobject>
<imagedata fileref="img/Conway_Brainb_result.png"/>
</imageobject>
<textobject>
<phrase>Ugyanez szöveges formátumban</phrase>
</textobject>
</mediaobject>
</figure>


    </section>        
            

    <section>
        <title>Vörös Pipacs Pokol/19 RF</title>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/_GCeJY2oDNo">https://youtu.be/_GCeJY2oDNo</link>      
        </para>
        <para>
            Megoldás forrása: 
<link xlink:href="Conway/RFH2.py">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Conway/RFH2.py</filename>
</link>              
        </para>
        <para>
            Az RFH2 döntőre, Katona Andreával közösen tervezett kódot szeretném itt bemutatni, ami az nb4tf4i_red_flower_hell_basic_d_sense.py szkriptre alapoz. A cél a program megírásakor az lett volna, hogy a láva előtt közvetlen egy árkot ásunk, amiben megreked a láva, így kényelmesen fel lehet venni az árok alatti összes virágot. Mindez egész jó ötletnek bizonyult, ám egy dolog miatt az egész terv füstbe ment. Az egész egyetlen apró figyelmetlenség eredménye: 300 mp van a feladat végrehajtására. Viszont, még mielőtt ezt megtapasztalhattuk volna, szorgosan dolgoztunk a szkript megírásán. 
        </para>  
<para>
Végül a szóban forgó időkorlát miatt nem fejeztük be a kódot, ám végeztünk egy próbafuttatást úgy, hogy az xml-ben átírtuk az időtartamot. Amint az a videóban is látszik, futás közben egy-egy helyen át kellett venni a vezérlést (akkor nem láttuk értelmét tökéletesíteni a kódot), de egészen sok virágot felvett az ágens. Egyébként a kód meglehetősen instabil, lett volna mit alakítani rajta. E helyett, az RFH 3 döntőjére teljesen más alapokról indultunk, a körbeásás gondolatát végleg elvetettük. De lássuk, mit is tartalmazott a kód.
</para>

<programlisting language="Python"><![CDATA[%
                dirt = 0

                if (begin == 0):
                    for i in nbr3x3x3:
                        if (i == 'dirt'):
                            dirt += 1

                    if (dirt > 9):
                        self.agent_host.sendCommand("jumpmove 1")
                        meddig += 1
                        time.sleep(.00001)
                    else:
                        self.agent_host.sendCommand("move 1")
                        time.sleep(.00001)

                        #meddig = 84 még fasza
                    if (meddig == meddigmenjen):
                        self.agent_host.sendCommand("move 1")
                        time.sleep(.05)
                        begin += 1
                        self.agent_host.sendCommand("turn -1")
                        time.sleep(.05)
                        self.agent_host.sendCommand("attack 1")
                        time.sleep(.1)
                        self.agent_host.sendCommand("look -1")
                        time.sleep(.05)
                        dirtosszes += dirt
]]></programlisting>

<para>
Kezdő lépésként (a begin funkciója hasonló az előbbi malmo-s projekthez) a felmenetelt kellett megoldani. Ezt a jump és jumpmove parancsokkal, a szokásos módon hajtottuk végre. Ha elértük a meddigmenjen változó értékét a meddig változóval (ami minden jumpmove után nő 1-el), elfordulunk és elkezdjük az ásást. Ekkor nő a begin változó értéke 1-el, ami azt jelenti, hogy a következő folyamat kezdődik el, ami a körbeásás.
</para>

<programlisting language="Python"><![CDATA[%
                #Part 2
                dirt = 0
                air = 0
                if (begin == 1):
                    for i in range(len(nbr3x3x3)-9, len(nbr3x3x3)):
                        if (nbr3x3x3[i] == 'dirt'):
                            dirt += 1

                    for i in range(0, len(nbr3x3x3)):
                        if (nbr3x3x3[i] == 'air'):
                            air += 1

                    '''Ha befejezte a dúrást'''
                    if ((fordulas > 3) and (air < 6)):
                        begin += 1
                        self.agent_host.sendCommand("move 1")
                        time.sleep(.1)
                        self.agent_host.sendCommand("move 1")
                        time.sleep(.1)
                        self.agent_host.sendCommand("jumpmove 1")
                        time.sleep(.1)
                        self.agent_host.sendCommand("turn -1")
                        time.sleep(.1)
                        self.agent_host.sendCommand("jumpmove 1")
                        time.sleep(.1)
                        self.agent_host.sendCommand("move 1")
                        time.sleep(.1)
                        self.agent_host.sendCommand("turn 1")
                        time.sleep(.1)
                        #self.agent_host.sendCommand("look 1")
                        #time.sleep(.1)
                        self.agent_host.sendCommand("move 1")
                        time.sleep(.1)
                        #self.agent_host.sendCommand("look 1")
                        #time.sleep(.1)

                    '''Ha fordulni kő'''
                    if (dirt == 5 and begin == 1):
                        fordulas += 1
                        self.agent_host.sendCommand("turn -1")
                        time.sleep(.01)
                        self.agent_host.sendCommand("move 1")
                        time.sleep(.01)
                        self.agent_host.sendCommand("attack 1")
                        time.sleep(.01)
                        self.agent_host.sendCommand("move 1")
                        time.sleep(.01)
                        self.agent_host.sendCommand("move 1")
                        time.sleep(.01)
                        self.agent_host.sendCommand("attack 1")
                        time.sleep(.01)
                        self.agent_host.sendCommand("move 1")
                        time.sleep(.01)
                        self.agent_host.sendCommand("move 1")
                        time.sleep(.01)
                        self.agent_host.sendCommand("attack 1")
                        time.sleep(.01)
                        self.agent_host.sendCommand("move 1")
                        time.sleep(.01)
                        self.agent_host.sendCommand("move 1")
                        time.sleep(.01)
                    else:
                        if (begin == 1):
                            self.agent_host.sendCommand("move 1")
                            time.sleep(.005)
                            self.agent_host.sendCommand("attack 1")
                            time.sleep(.005)
                        if ('red_flower' in nbr3x3x3):
                            time.sleep(.5)
                        #self.agent_host.sendCommand("move 1")
                        #time.sleep(.005)
                    #if (fordulas > 3):
                        #megmennyit += 1
                    #if (megmennyit > meddigmenjen*(11/8)+2):

                    #if (nbr3x3x3[12] == 'air' and fordulas > 3):


                    self.agent_host.sendCommand("move 1")
                    time.sleep(.005)
                    #if (nbr3x3x3[12] == 'air'):
                        #begin += 1

]]></programlisting>
          
<para>
Ez a második rész a körbeásás folyamatát írja le. A feltételek, hogy mikor forduljon az ágens, stb eléggé kiforratlanok, általában a harmadik futásra sikerült elérni azt, amit szerettünk volna. De egy-egy ilyen sikert nagy örömködés övezett, ugyanis ha az ágens megásta az árkot, és ki is tudott jönni belőle, az azt jelentette, hogy utána akármilyen tempóban be lehet gyűjteni a virágokat. Ezt már a következő rész írja le:
</para>

<programlisting language="Python"><![CDATA[%
                if (begin == 2):
                    dirt = 0
                    rf = 0
                    for i in nbr3x3x3:
                        if (i == 'red_flower'):
                            rf += 1
                            #self.agent_host.sendCommand("move -1")
                            #time.sleep(.1)
                            self.agent_host.sendCommand("look 1")
                            time.sleep(.1)
                            self.agent_host.sendCommand("attack 1")
                            time.sleep(.5)
                            self.agent_host.sendCommand("turn -1")
                            time.sleep(.1)
                            self.agent_host.sendCommand("jumpmove 1")
                            time.sleep(.1)
                            self.agent_host.sendCommand("attack 1")
                            time.sleep(.5)
                            self.agent_host.sendCommand("jumpmove 1")
                            time.sleep(.1)
                            self.agent_host.sendCommand("turn 1")
                            time.sleep(.1)
                            self.agent_host.sendCommand("move -1")
                            time.sleep(.1)
                            self.agent_host.sendCommand("move -1")
                            time.sleep(.1)
                            self.agent_host.sendCommand("look -1")
                            time.sleep(.1)
                            self.agent_host.sendCommand("move 1")
                            time.sleep(.01)
                            self.agent_host.sendCommand("move 1")
                            time.sleep(.01)
                            sensations = world_state.observations[-1].text
                            observations = json.loads(sensations)
                            nbr3x3x3 = observations.get("nbr3x3", 0)

                    for i in range(0, 18):
                        if (nbr3x3x3[i] == 'dirt'):
                            dirt += 1


                    if ((dirt == 14 or dirt == 13) and rf == 0):
                        self.fordul2(-1) #Balra fordul és elindul (-1)
                    else:
                        self.agent_host.sendCommand("move 1")
                        time.sleep(.0001)
]]></programlisting>

<para>
Ez az a rész, amit nem dolgoztunk ki teljesen, mivel az xml-ben foglalt időkorlát közbeszólt. Minden esetre egy viszonylag egyszerű mozgáskombinációval igyekeztünk felvenni a virágot, amikor az éppen az érzékelés tartományába esett. 
</para>

    </section>




                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
</chapter>                
