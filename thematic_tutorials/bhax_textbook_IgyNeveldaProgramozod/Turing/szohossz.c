#include <stdio.h>

int main() {

	int i = 1;
	int bit = 0;

	while (i != 0) {
		i <<= 1;
		bit+=1;
	}

	printf("A szóhossz: %d bit.\n", bit);
	return 0;
}
