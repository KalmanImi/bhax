#include <stdio.h>
#include <curses.h>
#include <unistd.h>
#include <math.h>	//Kell az abszolút érték kiszámolásához

int main ( void )
{
    WINDOW *ablak;
    ablak = initscr ();

    int x = -1;		//Táblán kívül esik 
    int y = -1;		// -||-
	int n = 1;		//Ezen a természetes számon végzem a maradékos osztást

    int mx;		//Maxi érték x részéről
    int my;		//Maxi érték y részéről

    for ( ;; ) {

        getmaxyx ( ablak, my , mx );	//Lekérem a maxi értékeket

        mvprintw ( y, x, "O" );		//Kiírom x-et és y-t

        refresh ();		
        usleep ( 100000 );
		
		x = abs((n%((mx-1)*2))-(mx-1));		//x koordinátát megadom
		y = abs((n%((my-1)*2))-(my-1));		//y koordinátát megadom
		n = n + 1;	//növelem minden 'képkockánál' n értékét
    }

    return 0;
}
