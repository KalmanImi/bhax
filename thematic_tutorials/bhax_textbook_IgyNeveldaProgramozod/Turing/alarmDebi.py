import urllib.request
from PIL import Image
import os
from time import gmtime, strftime
import time
import winsound

frequency = 500
duration = 300

pos1 = 100
pos2 = 124

#Debi: 143,89,8
x = ['143']
y = ['89']

while True:
    for i in x:
        for j in y:
            #print(i,j)
            link = 'https://tiles.lightningmaps.org/?x='+i+'&y='+j+'&z=8&s=256&t=5&T=12992604'
            file = i+j+'.png'
            urllib.request.urlretrieve(link,file)
            im = Image.open(file)
            pix = im.load()
            n = 0
            for k in range(pos1-100,pos1+101):
                for l in range(pos2-100,pos2+101):
                    if pix[k,l] ==5 or pix[k,l] == 6:
                        n = n+1
                    if k == pos1-100 or k == pos1+100 or l == pos2-100 or l == pos2+100:
                        pix[k,l] = 8
                    elif k == pos1 and l == pos2:
                        pix[k,l] = 8
            if n != 0:
                for k in range(1,5):
                    winsound.Beep(frequency*k, duration)
            print(strftime("%Y-%m-%d %H:%M:%S", gmtime())+'   '+i+'-'+j+': '+str(n))
            im.save(i+j+'.png')
            im.close()
            #os.remove(file)
    time.sleep(120)


