<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Caesar!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>









    <section>
        <title><type>double **</type> háromszögmátrix</title>
        <para>
            Írj egy olyan <function>malloc</function> és <function>free</function>
            párost használó C programot, amely helyet foglal
            egy alsó háromszög mátrixnak a szabad tárban!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/1MRTuKwRsB0">https://youtu.be/1MRTuKwRsB0</link>,
            <link xlink:href="https://youtu.be/RKbX5-EWpzA">https://youtu.be/RKbX5-EWpzA</link>.
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Caesar/tm.c">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Caesar/tm.c</filename>
            </link> 
        </para>
	<para>
		Védési videó:
<link xlink:href="https://youtu.be/zdK3ChmKFs4">https://youtu.be/zdK3ChmKFs4</link>

	</para>

        <para>
            Ez a feladat, az előző fejezetre alapozva, a pointerek világába vezet be minket. A feladat tulajdonképpen pointerek segítségével egy háromszögmátrix megalkotása. A háromszögmátrix olyan négyzetes mátrix (az oszlop- és sorszám megegyezik), ahol a főátló alatti vagy feletti rész csupa 0-kból áll, az értékes adatok a mátrix többi részét képezik. A háromszögmátrixnak két fő fajtáját különíthetjük el: az alsót és a felsőt. Az alsó esetében a mátrix feltöltése először balról jobbra, majd lefelé haladva történik, úgy, hogy mindegyik sorba egyel több elemet szúrok be. Így a mátrix bal alsó részén lesznek a fontos adatok. Ezzel szemben a felső háromszög-mátrix esetében fentről lefelé, majd balról jobbra haladok, így az értékes adatok a jobb felső sarokba kerülnek. A mátrixok ábrázolásáról készítettem is egy ábrát, Blenderben:
        </para>
        <figure>
            <title>Az alsó- (balra) és a felső háromszögmátrix szerkezete, feltöltése adatokkal.</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/haromszogmatrix_kicsi.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>Az alsó- (balra) és a felső háromszögmátrix szerkezete, feltöltése adatokkal.</phrase>
                </textobject>
            </mediaobject>
        </figure>
        <para>
            Jelen esetben egy helytakarékosan ábrázolt alsó háromszögmátrixot képezünk, nem is akármilyen módon: egy egészre mutató mutató mutatójával. Erre egy c program áll a rendelkezésünkre, lássuk a tartalmát:
        </para>

        <programlisting language="c"><![CDATA[#include <stdio.h>
#include <stdlib.h>

int
main ()
{
    int nr = 7;
    double **tm;

    printf("%p\n", &tm);
]]></programlisting>
<para>
Első körben beinkludáljuk a megfelelő header fájlokat. A malloc utasításhoz az stdlib.h header inkludálása is szükséges. Az nr megadja a sorok, egyben az oszlopok számát is. Ezt az értéket 7-re állítottam. A double **tm a szóban forgó pointer, melyet deklarálunk. Ezt követően kiíratjuk a tm memóriacímét.
</para>
<programlisting language="c"><![CDATA[

    if ((tm = (double **) malloc (nr * sizeof (double *))) == NULL)
    {
        return -1;
    }
    printf("%p\n", tm);
]]></programlisting>
<para>
A malloc hívásával lefoglalunk 7*8 bájtnyi területet. A malloc, hívásakor void* típusú mutatót ad vissza, így típuskényszerítéssel bármilyen típusú értéket képes visszaadni (jelen esetben double**). Ha a tárhelyfoglalás nem sikerült, visszatérünk -1 értékkel (hibával), ellenkező esetben kiíratjuk a lefoglalt tár címét. 
</para>
<programlisting language="c"><![CDATA[

    for (int i = 0; i < nr; ++i)
    {
        if ((tm[i] = (double *) malloc ((i + 1) * sizeof (double))) == NULL)
        {
            return -1;
        }

    }

]]></programlisting>
<para>
Hasonlóan az előzőhöz, itt is tárhelyfoglalás történik. A különbség abban rejlik, hogy a fenti, sorindexekre mutató mutatókhoz most lefoglalunk akkora területeket, amennyi elem egy-egy sorban jelen lesz. Azaz tulajdonképpen ezzel a lépéssel fogjuk az adatoknak a tárhelyet lefoglalni. Az első sorban egy double*-nyi, a másodikban 2, stb helyet foglalunk le. 
</para>
<programlisting language="c"><![CDATA[
    for (int i = 0; i < nr; ++i)
        for (int j = 0; j < i + 1; ++j)
            tm[i][j] = i * (i + 1) / 2 + j;
]]></programlisting>
<para>
Ezen for ciklusok keretein belül végezzük el az alaptábla feltöltését adatokkal. A tm[i][j] a mátrix i. sorának j. elemét jelenti.
</para>
<programlisting language="c"><![CDATA[
    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%f, ", tm[i][j]);
        printf ("\n");
    }
]]></programlisting>
<para>
És végül, hogy lássuk, jól dolgoztunk-e, megkíséreljük kiíratni a mátrixot. Mindezt úgy tesszük, hogy minden sorindex növekedésnél egy soremelést illesztünk a forráskódba, egyébként vesszővel választjuk el az elemeket. 
</para>
<programlisting language="c"><![CDATA[
    tm[3][0] = 42.0;
    (*(tm + 3))[1] = 43.0;	// mi van, ha itt hiányzik a külső ()
    *(tm[3] + 2) = 44.0;
    *(*(tm + 3) + 3) = 45.0;
]]></programlisting>
<para>
Ezekben a sorokban az eredeti mátrix 4. sorának elemeit megváltoztatjuk, különböző felírási alakokban. C-ben kétféleképpen is hivatkozhatunk tömb elemeire. Az egyik, amikor mutató segítségével tesszük meg mindezt (4. eset), a másik, amikor tömbindexekkel(1. eset). Ez utóbbi alkalmazása számomra sokkal átláthatóbb, kényelmesebb. Természetesen lehetőség adódik a kevert használatra is (2. és 3. eset). Fontos megjegyezni, ha C-ben dolgozunk, helytelen tömbelem-hivatkozásnál nem kapunk 'Out of range' hibaüzenetet, ami szemantikai hibákat generálhat.
</para>
<programlisting language="c"><![CDATA[
    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%f, ", tm[i][j]);
        printf ("\n");
    }
]]></programlisting>
<para>
Miután a 4. sor elemeit lecseréltük, szintén kiíratjuk a mátrixot.
</para>
<programlisting language="c"><![CDATA[
    for (int i = 0; i < nr; ++i)
        free (tm[i]);

    free (tm);

    return 0;
}]]></programlisting>
<para>
És végül felszabadítjuk a tárhelyet a free() függvény segítségével. A program fordításáról, futtatásáról mellékeltem egy képet: 
</para>
<figure>
            <title>A program fordítása és futtatása</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/Caesar_haromszogmat.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>A program fordítása és futtatása</phrase>
                </textobject>
            </mediaobject>
        </figure> 
<para>
A folyamat ábrán megjelenítve:
</para>
        <figure>
            <title>A <type>double **</type> háromszögmátrix a memóriában</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/doublecscs.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>A <type>double **</type> háromszögmátrix a memóriában</phrase>
                </textobject>
            </mediaobject>
        </figure>                             
        <para>
            Magáról a pointerek működéséről az itt feltüntetett ábra ad részletesebb magyarázatot, a programhoz igazodva. A középső sorban a sorindexek szerepelnek. Ezekre mutat a tm mutató, és ezen mutatók mutatnak az alsó sorban elhelyezkedő konkrét értékekre. Ennek megfelelően, a középső sorban szereplő elemek adják a sor-, az alsóban lévők az oszlopindexet.
        </para>            
    </section> 









       
    <section>
        <title>C EXOR titkosító</title>
        <para>
            Írj egy EXOR titkosítót C-ben!
        </para>
	<para>
        Feladatunk, hogy egy olyan titkosítót hozzunk létre, amely a kizáró vagy logikai műveleten alapul. 
	</para>
        <para>
            Megoldás videó:
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Caesar/e.c">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Caesar/e.c</filename>
            </link>               
        </para>

<programlisting language="c"><![CDATA[
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#define MAX_KULCS 100
#define BUFFER_MERET 256


]]></programlisting>
<para>
Miután a megfelelő dolgokat beinkludáltuk, megadjuk a MAX_KULCS és a BUFFER_MERET konstansok értékét. Ezek a később argumentumként megadott kulcs, illetve a buffer méretének maximumát írják le, amelybe a titkosítandó szöveg kerül bele.
</para>
<programlisting language="c"><![CDATA[
int
main (int argc, char **argv)
{

  char kulcs[MAX_KULCS];
  char buffer[BUFFER_MERET];

  int kulcs_index = 0;
  int olvasott_bajtok = 0;

  int kulcs_meret = strlen (argv[1]);
  strncpy (kulcs, argv[1], MAX_KULCS);

]]></programlisting>
<para>
Ebben a részben létrehozunk a kulcs- és a később standard inputról beolvasott karaktersorozat tárolására egy-egy tömböt. Továbbá inicializáljuk a kulcs_index és az olvasott_bajtok változókat. Ez előbbi a kulcs tömb elemein történő végigjárást fogja biztosítani, utóbbi a beolvasott bájtokat (karaktereket) számolja.
</para>
<programlisting language="c"><![CDATA[
  while ((olvasott_bajtok = read (0, (void *) buffer, BUFFER_MERET)))
    {

      for (int i = 0; i < olvasott_bajtok; ++i)
	{

	  buffer[i] = buffer[i] ^ kulcs[kulcs_index];
	  kulcs_index = (kulcs_index + 1) % kulcs_meret;

	}

      write (1, buffer, olvasott_bajtok);

    }
}
]]></programlisting>
<para>
Ebben a részben történik meg a titkosítás konkrét folyamata. Az olvasás közvetlenül a standard inputról történik. A beolvasott bájtokra void * mutató mutat, azaz bármilyen típust rájuk kényszeríthetünk. Jelen esetben karakterekkel dolgozunk. A függvény utolsó paramétere korlátozza le az olvasást, ha megtelt az aktuális buffer.            
</para>            
<para>
A következő for ciklusnál végigmegyünk az eddig beolvasott bájtokon, és az összes beolvasott karakter bitjein elvégezzük a kizáró vagy műveletet. A művelet végrehajtásánál mindig az aktuális karakter értékét változtatjuk, az aktuális karakter és a kulcs kulcs_index-edik bitsorozata alapján. A kulcs_index értéke minden új karakternél egyel nő, egészen addig, amíg el nem érjük a kulcs hosszát. A hiba elkerülése érdekében a számolást ekkor újrakezdjük. Végül kiíratjuk az átalakított karaktersorozatot.
</para>
<para>
Végül lássunk egy példát a program konkrét futására:
</para>
        <figure>
            <title>Az exor titkosító fordítása, egy konkrét futása.</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/c_exor.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>Az exor titkosító fordítása, egy konkrét futása.</phrase>
                </textobject>
            </mediaobject>
        </figure>
    </section> 










       
    <section>
        <title>Java EXOR titkosító</title>
        <para>
            Írj egy EXOR titkosítót Java-ban!
        </para>
        <para>
A Java programozási nyelv objektum-orientáltsága révén inkább hasonlít az objektum orientáltságot szintén támogató C++-ra, mint a C-re. Elsőre szokatlan lehet az osztályok jelenléte, de érdemes belátni, hogy a nagyobb projektek létrehozásánál - többek között az átláthatóság megőrzése érdekében - elengedhetetlen ez a szemléletmód.
        </para>
<para>
Ugyan a jelen program megírásakor még nem használjuk ki a classok adta lehetőségeket, lássuk, hogy alapjaiban mi a különbség a c- és a Java nyelven megírt kódcsipetek között! A Java kódot egy osztály létrehozásával kezdjük, amelyen belül létrehozzuk a main() metódust. Mindezt a következő kód beírásával tehetjük meg:
</para>
<programlisting language="Java"><![CDATA[
public static void main(String[] args) {}
]]></programlisting>
<para>
Ebben a sorban a public kulcsszó a bármely objektum számára látható és elérhető tulajdonságot jelenti, a static kulcsszó egyediséget, azaz hogy nem része egyetlen másik beágyazott objektumnak sem. A void arra utal, hogy nem várunk visszatérési értéket. A paraméterlistában az argumentumok szerepelnek, melyek String-ként kerülnek beolvasásra.
</para>
        <para>
            Megoldás videó:
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Caesar/Exor.java">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Caesar/Exor.java</filename>
            </link>                
        </para>
   
<programlisting language="Java"><![CDATA[         
public class Exor {
    
    public Exor(String kulcsSzöveg,
            java.io.InputStream bejövőCsatorna,
            java.io.OutputStream kimenőCsatorna)
            throws java.io.IOException {
        
        byte [] kulcs = kulcsSzöveg.getBytes();
        byte [] buffer = new byte[256];
        int kulcsIndex = 0;
        int olvasottBájtok = 0;

        while((olvasottBájtok =
                bejövőCsatorna.read(buffer)) != -1) {
            
            for(int i=0; i<olvasottBájtok; ++i) {
                
                buffer[i] = (byte)(buffer[i] ^ kulcs[kulcsIndex]);
                kulcsIndex = (kulcsIndex+1) % kulcs.length;
                
            }
            
            kimenőCsatorna.write(buffer, 0, olvasottBájtok);
            
        }
        
    }
]]></programlisting>
<para>
Kódunk megírását kivitelezhetnénk úgy is, hogy mindent a main()-be írunk, viszont annak előbb-utóbb spagettikód lenne az eredménye. Így hát létrehozunk egy olyan függvényt, amely megkapja paraméterként a bejövő sorozatot, a kimenő sorozatot, illetve a kulcsszöveget. Hasonlóan a C nyelvű kódunkhoz, bevezetünk két változót, amelyek közül az egyik a kulcs aktuális pozícióját írja le, a másik pedig az eddig beolvasott bájtok számát. Az eljárás menete nagyon hasonlít a C programéhoz, így külön nem szeretném részletezni. 
</para>
<programlisting language="Java"><![CDATA[  
    public static void main(String[] args) {
        
        try {
            
            new Exor(args[0], System.in, System.out);
            
        } catch(java.io.IOException e) {
            
            e.printStackTrace();
            
        }
        
    }
    
}
]]></programlisting>
	<para>
A main metódusban a try-catch páros a Java nyelvben megszokott hibakezelést írja le. Python nyelven hasonló páros a try-except. Ebben az esetben megpróbáljuk létrehozni az Exor objektumot, úgy, hogy paraméterként az első argumentumot, a standard inputot és outputot adjuk meg. Ha az Exor objektum létrehozása meghiúsul, és az IOException hiba lépett fel, akkor a program leáll, és megkapjuk a hibaüzenetet. A következőkben lássuk, hogyan fordítható le és futtatható a program, majd egy korrekt és egy inkorrekt kulcs megadásakor mi történik, ha az eredeti szöveget szeretnénk visszakapni! 
        </para>  
<para>
A Java forráskód fordítása és futtatása különbözik az eddig megszokottaktól. Először ugyanis egy platformfüggetlen bájtkód generálódik a fordítás során (.class), a forrásszöveg alapján. Ezt követően jön csak létre az aktuális platformon futtatható állomány. 
</para>    
<figure>
            <title>A Java titkosító fordítása és futtatása</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/javaexor.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>A Java titkosító fordítása és futtatása</phrase>
                </textobject>
            </mediaobject>
        </figure>
<para>
A kód első futtatásakor kulcsként az 1234 értéket adjuk meg, és a titkosított szöveget átirányítjuk a titkosított.szöveg állományba. A képen látható, hogy a titkosított szöveget így nem értelmezhető. Ha ugyanezt a műveletet elvégezzük az előbb megadott kulccsal, visszakapjuk az eredeti forrásszöveget. Ezt a program következő futtatása reprezentálja. Majd úgy is lefuttatjuk a programot, hogy a kulcsot megváltoztatjuk. Nem kapjuk vissza az eredeti szöveget, tehát ez felér egy helytelen próbálkozással. Amennyiben a kulcsot nem ismerjük, egyetlen lehetőségünk marad arra, hogy olvashatóvá tegyük a számunkra nem elérhető szöveget: egy jól működő feltörőprogram létrehozása. Természetesen ennek megvannak a maga korlátai is, de ezeket lássuk a következő feladatban. 
</para>  
    </section>  









      
    <section xml:id="bhax-textbook-feladatok-caesar.EXOR-toro">
        <title>C EXOR törő</title>
        <para>
            Írj egy olyan C programot, amely megtöri az első feladatban előállított titkos szövegeket!
        </para>
<para>
Az objektumok világából visszalátogatunk az eljárásközpontú pragmatikára. Mi történik abban az esetben, ha a kizáró vagy műveletre alapozó titkosító eljárás által generált titkos szöveget szeretnénk újra elolvasni, de nem ismerjük vagy elfelejtettük a hozzá megadott kulcsot? Elöljáróban annyit, hogy akármit is teszünk, bizonyára sok időbe telik a folyamat, még a jelenlegi számítógépek kiemelkedő számítási kapacitása ellenére is.
</para>
        <para>
            Megoldás videó:
        </para>
        <para>
            Megoldás forrása:  <link xlink:href="Caesar/t.c">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Caesar/t.c</filename>
            </link>                
        </para>              
        <para>
            Lássuk is a programkódot.
        </para>  

<programlisting language="c"><![CDATA[
#define MAX_TITKOS 4096
#define OLVASAS_BUFFER 256
#define KULCS_MERET 8
#define _GNU_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <string.h>

]]></programlisting>
<para>
Először is bevezetünk pár konstans értéket, majd inkludáljuk a feladat megoldásához szükséges dolgokat.
</para>
<programlisting language="c"><![CDATA[

double
atlagos_szohossz (const char *titkos, int titkos_meret)
{
    int sz = 0;
    for (int i = 0; i < titkos_meret; ++i)
        if (titkos[i] == ' ')
            ++sz;

    return (double) titkos_meret / sz;
}

]]></programlisting>
<para>
Az első függvény, amelyet létrehozunk, a szavak átlagos hosszát fogja nekünk megadni, ami a titkos szöveget illeti. A függvény bemenetként megkapja a titkos szöveget és annak hosszát. Az sz értékének egy for cikluson átívelve megadjuk a szóközök számát. Visszatérítési értékként a titkos szöveg hosszának és a szóközök számának hányadosát adjuk meg, vagy másképpen az átlagos szóhosszt. 
</para>
<programlisting language="c"><![CDATA[

int
tiszta_lehet (const char *titkos, int titkos_meret)
{
    // a tiszta szoveg valszeg tartalmazza a gyakori magyar szavakat
    // illetve az átlagos szóhossz vizsgálatával csökkentjük a
    // potenciális töréseket

    double szohossz = atlagos_szohossz (titkos, titkos_meret);

    return szohossz > 6.0 && szohossz < 9.0
           && strcasestr (titkos, "hogy") && strcasestr (titkos, "nem")
           && strcasestr (titkos, "az") && strcasestr (titkos, "ha");

}

]]></programlisting>
<para>
A tiszta_lehet függvény az előző függvény által megkapott átlagos szóhosszt használja fel. Tulajdonképpen azt vizsgálja meg, hogy a fejtésben lévő kód tiszta-e már. Visszatérítési értéke ennek tükrében 0 vagy 1 lesz (tiszta vagy nem).
</para>
<programlisting language="c"><![CDATA[

void
exor (const char kulcs[], int kulcs_meret, char titkos[], int titkos_meret)
{

    int kulcs_index = 0;

    for (int i = 0; i < titkos_meret; ++i)
    {

        titkos[i] = titkos[i] ^ kulcs[kulcs_index];
        kulcs_index = (kulcs_index + 1) % kulcs_meret;

    }

}

]]></programlisting>
<para>
E az eljárás ugyanazt csinálja, mint a titkosító programunk fő eleme a titkosításnál. Exor művelettel próbálja előállítani az eredeti szöveget.
</para>
<programlisting language="c"><![CDATA[

int
exor_tores (const char kulcs[], int kulcs_meret, char titkos[],
            int titkos_meret)
{

    exor (kulcs, kulcs_meret, titkos, titkos_meret);

    return tiszta_lehet (titkos, titkos_meret);

}

]]></programlisting>
<para>
Ez a függvény vizsgálja meg, hogy ténylegesen tiszta-e a kód. Az előzőekben bemutatott exor és tiszta_lehet függvények végrehajtásával érjük el mindezt. 
</para>
<programlisting language="c"><![CDATA[

int
main (void)
{

    char kulcs[KULCS_MERET];
    char titkos[MAX_TITKOS];
    char *p = titkos;
    int olvasott_bajtok;

]]></programlisting>
<para>
Eljött a main metódus hívásának ideje. Rögtön deklaráljuk is a kulcs és titkos tömböt, melyek méretét a fent definiált konstansok adják meg. A p mutató a titkos tömb első elemére mutat. Az olvasott_bajtok a beolvasásnál lesz használatos.
</para>
<programlisting language="c"><![CDATA[

    while ((olvasott_bajtok =
                read (0, (void *) p,
                      (p - titkos + OLVASAS_BUFFER <
                       MAX_TITKOS) ? OLVASAS_BUFFER : titkos + MAX_TITKOS - p)))
        p += olvasott_bajtok;

]]></programlisting>
<para>
A bájtok beolvasása itt történik, egészen addig, amíg a végére nem érünk vagy a titkos buffer be nem telik.
</para>
<programlisting language="c"><![CDATA[

    // maradek hely nullazasa a titkos bufferben
    for (int i = 0; i < MAX_TITKOS - (p - titkos); ++i)
        titkos[p - titkos + i] = '\0';

]]></programlisting>
<para>
A titkos buffer megmaradt helyeit kinullázzuk. 
</para>
<programlisting language="c"><![CDATA[

    // osszes kulcs eloallitasa
    for (int ii = '0'; ii <= '9'; ++ii)
        for (int ji = '0'; ji <= '9'; ++ji)
            for (int ki = '0'; ki <= '9'; ++ki)
                for (int li = '0'; li <= '9'; ++li)
                    for (int mi = '0'; mi <= '9'; ++mi)
                        for (int ni = '0'; ni <= '9'; ++ni)
                            for (int oi = '0'; oi <= '9'; ++oi)
                                for (int pi = '0'; pi <= '9'; ++pi)
                                {
                                    kulcs[0] = ii;
                                    kulcs[1] = ji;
                                    kulcs[2] = ki;
                                    kulcs[3] = li;
                                    kulcs[4] = mi;
                                    kulcs[5] = ni;
                                    kulcs[6] = oi;
                                    kulcs[7] = pi;

                                    if (exor_tores (kulcs, KULCS_MERET, titkos, p - titkos))
                                        printf
                                        ("Kulcs: [%c%c%c%c%c%c%c%c]\nTiszta szoveg: [%s]\n",
                                         ii, ji, ki, li, mi, ni, oi, pi, titkos);

                                    // ujra EXOR-ozunk, igy nem kell egy masodik buffer
                                    exor (kulcs, KULCS_MERET, titkos, p - titkos);
                                }

    return 0;
}
]]></programlisting>

<para>
Végezetül az összes lehetséges kombinációt előállítjuk (a for ciklusok), majd megvizsgáljuk a végén, hogy megkaptuk-e a tiszta szöveget. Ez utóbbit a már említett exor függvény hívásával tesszük meg. Jelen kódtörő hátránya, hogy csak számokból álló kulcsot tud kezelni, feltörni. 
</para>
<para>
A kód kielemzése után lássunk egy konkrét futást is. A várható hosszas futás miatt egy rövid, csupán számokból álló kulcsot adunk meg (egyébként is csak számokon alapuló törés lehetséges jelen program esetében). Ha kibővítenénk a karakterekre és egyéb jelekre is, a törésre befektetett idő jelentősen megnyúlna. Tovább növelné az időtartamot hosszabb kulcs megadása.
</para>
	<figure>
            <title>A titkok_kulcsa.txt állomány tartalmának visszafejtése, '12' kulcs esetén</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/exor_toro.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>A titkok_kulcsa.txt állomány tartalmának visszafejtése, '12' kulcs esetén</phrase>
                </textobject>
            </mediaobject>
        </figure> 
<para>
Kulcsnak '12' értéket adtunk meg korábban, a szöveg tartalma a titkok_kulcsa.txt állományban szerepel. A feltörés 25 percig tartott.
</para>
    </section>   






     
    <section>
        <title>Neurális OR, AND és EXOR kapu</title>
        <para>
            R
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/Koyw6IH5ScQ">https://youtu.be/Koyw6IH5ScQ</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Caesar/nn.r">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Caesar/nn.r</filename>
            </link>                        
        </para>
        <para>
            A következőkben a gépi tanulás világában kalandozunk. Ehhez az R programnyelv adta lehetőségeket aknázzuk ki. Feladatunk az alap logikai műveletek megtanítása. Jelen esetben a 'megengedő vagy', 'és', illetve a 'kizáró vagy' műveleteket fogjuk megtanítani a számítógépünkkel, neuronok működésének mintájára.
        </para>
	<para>
            Először fontos leszögezni, hogy hogyan is működnek a neuronok ....
        </para>

<programlisting language="R"><![CDATA[
#   Copyright (C) 2019  Dr. Norbert Bátfai, nbatfai@gmail.com
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>
#
# https://youtu.be/Koyw6IH5ScQ

library(neuralnet)

a1    <- c(0,1,0,1)
a2    <- c(0,0,1,1)
OR    <- c(0,1,1,1)

or.data <- data.frame(a1, a2, OR)

nn.or <- neuralnet(OR~a1+a2, or.data, hidden=0, linear.output=FALSE, stepmax = 1e+07, threshold = 0.000001)

plot(nn.or)

compute(nn.or, or.data[,1:2])

]]></programlisting>
<figure>
            <title>A betanulást követő eredmény a megengedő vagy logikai műveletre</title>
	<mediaobject>
		        <imageobject>
		            <imagedata fileref="img/neur1.png" scale="50" />
		        </imageobject>
		        <textobject>
		            <phrase>A betanulást követő eredmény a megengedő vagy logikai műveletre</phrase>
		        </textobject>
		    </mediaobject>
</figure>
<para>
Az első esetben a megengedő vagy logikai műveletet tanítjuk. Ebben az esetben csak akkor kapunk hamis állítást (azaz 0-t), ha a1 és a2 bemenő értéke is egyaránt hamis. Ha legalább az egyik igaz, igazat kapunk. Ezen logikai művelet betanítása után az elvégzett teszt a következő eredményt adta ki: Két hamis érték esetén 0.00125, ha az egyik hamis, a másik igaz, 0.9999, ha pedig mindegyik érték igaz, 1-et kapunk. Kerekítve kijön az elvárt érték, tehát a tanítás sikeres volt. A művelet 141 lépésnél végződik, igen csekély hibafaktorral.
</para>
<programlisting language="R"><![CDATA[

a1    <- c(0,1,0,1)
a2    <- c(0,0,1,1)
OR    <- c(0,1,1,1)
AND   <- c(0,0,0,1)

orand.data <- data.frame(a1, a2, OR, AND)

nn.orand <- neuralnet(OR+AND~a1+a2, orand.data, hidden=0, linear.output=FALSE, stepmax = 1e+07, threshold = 0.000001)

plot(nn.orand)

compute(nn.orand, orand.data[,1:2])

]]></programlisting>
<figure>
            <title>A betanulást követő eredmény a megengedő vagy  és az és logikai műveletekre</title>
	<mediaobject>
		        <imageobject>
		            <imagedata fileref="img/neur2.png" scale="50" />
		        </imageobject>
		        <textobject>
		            <phrase>A betanulást követő eredmény a megengedő vagy és az és logikai műveletekre</phrase>
		        </textobject>
		    </mediaobject>
</figure>
<para>
Ezt követően nemcsak a vagy, hanem az és logikai műveletet is betanítottuk, melynek sémáját és eredményeit az ábrán láthatjuk.
</para>
<programlisting language="R"><![CDATA[

a1      <- c(0,1,0,1)
a2      <- c(0,0,1,1)
EXOR    <- c(0,1,1,0)

exor.data <- data.frame(a1, a2, EXOR)

nn.exor <- neuralnet(EXOR~a1+a2, exor.data, hidden=0, linear.output=FALSE, stepmax = 1e+07, threshold = 0.000001)

plot(nn.exor)

compute(nn.exor, exor.data[,1:2])

]]></programlisting>
<figure>
            <title>A betanulást követő eredmény a kizáró vagy logikai műveletre</title>
	<mediaobject>
		        <imageobject>
		            <imagedata fileref="img/neur3.png" scale="50" />
		        </imageobject>
		        <textobject>
		            <phrase>A betanulást követő eredmény a kizáró vagy logikai műveletre</phrase>
		        </textobject>
		    </mediaobject>
</figure>
<para>
Nagy fordulatot jelentett az exor művelet betanítása. Eredményként ugyanis minden egyes kimenetel esetén jó közelítéssel 0.5-et kapunk, ami a teljes bizonytalanságot, avagy 0 tanulást szemlélteti. Ezen a ponton sokan elhatárolódtak a módszertől, viszont neves matematikusok megbirkóztak a problémával. Úgy sikerült ezen hibát kiküszöbölni, hogy rejtett neuronokat vezettek be. Ennek hatására a várt eredményt sikerült elérni, mint ahogyan az a következő kódcsipetben és mellékelt ábrán is megjelenik.
</para>
<programlisting language="R"><![CDATA[

a1      <- c(0,1,0,1)
a2      <- c(0,0,1,1)
EXOR    <- c(0,1,1,0)

exor.data <- data.frame(a1, a2, EXOR)

nn.exor <- neuralnet(EXOR~a1+a2, exor.data, hidden=c(6, 4, 6), linear.output=FALSE, stepmax = 1e+07, threshold = 0.000001)

plot(nn.exor)

compute(nn.exor, exor.data[,1:2])
]]></programlisting>

<figure>
            <title>A betanulást követő eredmény a kizáró vagy logikai műveletre rejtett neuronokkal korrigálva</title>
	<mediaobject>
		        <imageobject>
		            <imagedata fileref="img/neur4.png" scale="50" />
		        </imageobject>
		        <textobject>
		            <phrase>A betanulást követő eredmény a kizáró vagy logikai műveletre rejtett neuronokkal korrigálva</phrase>
		        </textobject>
		    </mediaobject>
</figure>
<para>
Jelen esetben a tanulást rejtett neuronok bevonásával segítettük. Ebben az esetben látványos javulás következett be, sőt, az és és a vagy logikai műveletekhez hasonlóan itt is pont azokat az eredményeket kaptuk, amiket vártunk. 
</para>
           
    </section>        
    <section>
        <title>Hiba-visszaterjesztéses perceptron</title>
        <para>
            C++
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/XpBnR31BRJY">https://youtu.be/XpBnR31BRJY</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://github.com/nbatfai/nahshon/blob/master/ql.hpp#L64">https://github.com/nbatfai/nahshon/blob/master/ql.hpp#L64</link>               
        </para>
        <para>
            Folytatjuk a neuron hálózatokkal kapcsolatos elmélkedést. A percetronok olyan algoritmusok, amelyek a számítógépnek megtanítják a bináris osztályozást. Az előző feladathoz hasonló a működési elv, vannak bemenő adatok adott súllyal, és ezek szorzatösszege alapján kapunk valamilyen kimenetet.
        </para>  
<para>
A folyamat első lépéseként a Mandelbrot halmazról egy png képet állítunk elő. Ehhez a mandelpng.cpp fájlt alkalmaztam, amelyet lefordítva és a -lpng kapcsolóval lefuttatva megkapom a kívánt png képet, a Mandelbrot halmazról:
</para>  

<figure>
            <title>A mandelpng.cpp fordítása, futtatása, és kimenetele</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/mandelout.png" scale="40" />
                </imageobject>
                <textobject>
                    <phrase>A mandelpng.cpp fordítása, futtatása, és kimenetele</phrase>
                </textobject>
            </mediaobject>
        </figure>
 
<para>
A program futtatása előtt a libpng++ csomagot kell telepíteni. Ezt követően a mellékelt .hpp kiterjesztésű fájl, és egy main.cpp forrásfájl segítségével tudjuk fordítani és lefuttatni a programot, a következő paranccsal: 
</para>  
<para>
g++ ml.hpp main.cpp -o perc -lpng
</para>
<para>
./perc mandel.png
</para>
<para>
A kapott eredmény nem feltétlen ugyanaz minden esetben. Egy példafordítás:
</para>
<figure>
            <title>A program fordítása, futtatása</title>
	<mediaobject>
		        <imageobject>
		            <imagedata fileref="img/Caesar_perc.png" scale="50" />
		        </imageobject>
		        <textobject>
		            <phrase>A program fordítása, futtatása</phrase>
		        </textobject>
		    </mediaobject>
</figure>

 
    </section>        
 
    <section>
        <title>Vörös Pipacs Pokol/írd ki, mit lát Steve</title>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/BpzY2hYPmAA">https://youtu.be/BpzY2hYPmAA</link>      
        </para>
        <para>
            Megoldás forrása: 
<link xlink:href="Caesar/ittegypipacs.py">
<filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Caesar/ittegypipacs.py</filename>
</link>             
        </para>
        <para>
            A Malmo ágensprogramozás egyik legizgalmasabb része következik. A jelen Python szkript ugyanis már nemcsak mozgásokat képes végrehajtani, hanem az xml-ben megadott értékek szerint képes a maga körüli közvetlen tér érzékelésére. Az xml fájlban a következő sorok jelennek meg, ami egy 3x3x3-as blokkot jelent Steve körül.
        </para>  

<programlisting language="xml"><![CDATA[

            <ObservationFromRay/>
                <ObservationFromHotBar/>
                    <ObservationFromFullStats/>
                    <ObservationFromGrid>
                        <Grid name="nbr3x3">
                            <min x="-1" y="-1" z="-1"/>
                            <max x="1" y="1" z="1"/>
                        </Grid>
                    </ObservationFromGrid> 
]]></programlisting>


<para>
A Python szkriptben a következő sorok kerültek a Steve osztály run() metódusába:  
</para>
<programlisting language="python"><![CDATA[
                sensations = world_state.observations[-1].text
                #print("    sensations: ", sensations)                
                observations = json.loads(sensations)
                nbr3x3x3 = observations.get("nbr3x3", 0)
                #print("    3x3x3 neighborhood of Steve: ", nbr3x3x3)
                    
                if "Yaw" in observations:
                    self.yaw = int(observations["Yaw"])
                if "Pitch" in observations:
                    self.pitch = int(observations["Pitch"])
                if "XPos" in observations:
                    self.x = int(observations["XPos"])
                if "ZPos" in observations:
                    self.z = int(observations["ZPos"])        
                if "YPos" in observations:
                    self.y = int(observations["YPos"]) 
]]></programlisting>

<para>
Ahol az nbr3x3x3 változó pontosan azt a 3x3x3-as blokkról szóló tömböt fogja tartalmazni, amit az xml-ben is látni lehetett. Közvetlen az ágens körüli teret. Azaz akkor érzékelünk pipacsot, ha megjelenik a 'red_flower' a 27 elemű nbr3x3x3 tömbben. Ezt a következőképpen fejezhetjük ki Python nyelven:
</para>

<programlisting language="python"><![CDATA[
if 'red_flower' in nbr3x3x3:
]]></programlisting>

<para>
Ez azonban nem minden, ugyanis azt is tudni kell, hogy éppen előtte, mögötte vagy éppen mellette van a pipacs. Ezt úgy érhetjük el, ha a tömb egy adott indexére hivatkozunk. És pontosan a tömb megfelelő elemére történő hivatkozással érhetjük el, hogy az jelenjen meg a képernyőn, amit Steve lát. Ezt az alábbi szkript-kibővítésben kifejezetten a piros pipacsra írtam meg, avagy ha virág kerül a fókuszba, akkor jelez a program:
</para>

<programlisting language="python"><![CDATA[
                if (nbr3x3x3[12] == "red_flower"):
                    print("Itt egy pipacs!")
                    self.agent_host.sendCommand( "attack 1" )
                    time.sleep(.5)
                self.agent_host.sendCommand( "move 1" )
                time.sleep(.1)
]]></programlisting>
          
<para>
Azaz ha a tömb 12. eleme pipacs, kiíratjuk, hogy "Itt egy pipacs!". Továbbá az attack 1 paranccsal meg is kíséreljük leszakítani azt. 
</para>

    </section>       
     



                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
</chapter>                
