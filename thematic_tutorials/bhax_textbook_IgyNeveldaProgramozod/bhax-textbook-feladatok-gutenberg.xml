<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Gutenberg!</title>
        <keywordset>
            <keyword/>
        </keywordset>
        <cover>
            <para>
                Programozás tankönyvek rövid olvasónaplói.
            </para>
        </cover>
    </info>
    <section>
        <title>Általánosan a könyvekről</title>
<para>         
Mindenek előtt dióhéjban szeretnék írni az olvasónapló alapját képező négy irodalomról. Első olvasásra úgy éreztem, vannak olyan könyvrészletek, amelyeket érdemes párhuzamosan olvasni (A Pici könyv és a Kernighan-Ritchie C könyve), van, ami az alapozást segíti (Pythonos könyv), és van, amelynek megértése előismereket igényel (C++ programozás). 
</para>
<para>
Ennek megfelelően, először a Python programozási nyelv elemeivel ismerkedtem meg. Mivel ez egy igen magasszintű programozási nyelv, nagyon egyszerű a használata. A "Helló Világ" kiíratása a konzolra csupán egy egysoros forráskóddal megvalósítható: 
</para>
<para>
print("Helló, Világ").
</para>
<para>
Hátránya, ami egyben nagy előnye is, hogy az interpreteres nyelvek csoportjába sorolható. Ez azt jelenti, hogy a fordítás-futtatás soronként történik. Azért hátrányos, mert így a program futása igen lassú, másrészt előnyös, mert ha hibát érzékelünk, a hibát tartalmazó sornál áll le a program. Így könnyen ki tudjuk javítani a felmerülő hibákat. Nagyobb algoritmusok tervezésénél érdemes lehet először jelen nyelven megfogalmazni a céljainkat, működő programot létrehozni, majd egy robusztusabb nyelvre átfordítani. Tehát, konklúzió a bevezetőben a Python nyelvről: gyorsan megírt kód, lassú futás.
</para>
<para>
Második körben a Pici könyvet, azaz Juhász Istvántól a Magasszintű programozási nyelvek I jegyzetet olvastam, azzal párhuzamosan a Kernighan-Ritchie-féle C programozási nyelvről szóló művét. Ez előbbi általánosan ír a programozási nyelvekről, beleértve az általunk használt C nyelvet. Rövid, tömör, lényegretörő jegyzet, amely nem példákon alapszik. A Kernighan-Ritchie-féle könyv inkább a programozási példákra fekteti a hangsúlyt. Konkrét példákon át vezet be minket a C programozás rejtelmeibe. A két mű együttes olvasása, példakódok futtatása nagyon jó lehetőséget teremt elmélyedni a programozás világában. 
</para>
<para>
Miután a nyelvi alapokkal jobban megismerkedtünk, célszerű betekintést nyerni a napjainkban oly sokat emlegetett objektumorientált programozási szemléletbe. Erre a C++ programozási nyelv nagyszerű lehetőséget ad számunkra. A nyelv elsajátítása azért is előnyös, mert több magasabb szintű nyelv hasonló elveken nyugszik, vagy éppen erre alapozódik. Továbbá egy olyan szemléletmódot ad a számunkra, ami gördülékenyebbé teheti a programírást, könnyebb a hibákat kijavítani. Egy nagyobb projekt esetén sokkal átláthatóbb kódot kapunk, ha objektumorientáltan írjuk. És még nagyon sok előnyt felsorolhatnánk. Viszont most térjünk rá az említett könyvek tartalmára, kicsit részletesebben. 
</para>
    </section>  
    <section>
        <title>Programozási alapfogalmak</title>
        <para>         
            <citation>PICI</citation>       
        </para>
<section>
<title>
A Python nyelvről
</title>
<para>
A Python nyelv magasszintű, gyorsan tanulható szkriptnyelv. A szkriptek futtatásához elegendő a python interpretert letölteni, mely több platformra is elérhető. Miután ezt megtettük, számos csomaggal lehet kiegészíteni az alapnyelvet, így gyorsan el tudunk végezni speciálisabb feladatokat is (például adatbázis-kezelés, ágens mozgatása a Minecraftban, stb.).
</para>
<para>
A python kód szerkesztése viszonylag egyszerű, szintaxisa behúzásalapú. Ez előnyös olyan szempontból, hogy a kód kötelezően átlátható, ám kellemetlen, ha elcsúsznak sorok a szerkesztés folyamán. Az utasítások után így nem kell végjelet alkalmaznunk.
</para>
<para>
A nyelv típusait nem részletezem, ugyanis nagyvonalakban megegyezik más programozási nyelvek típusaival. Ami viszont fontos, hogy a változók deklarálásánál nem írjuk ki külön a típust, annak bevezetését az interpreter végzi. Erre példa az x változó bevezetése, melynek értékül adjuk a 12-t: x=12. Ez C-ben így mutatna: int x = 12;
</para>
<para>
A sztringek idézőjelek között, valamint aposztrófok között is felírhatók. További típusok a listák, ennesek, szótárak.
</para>
<para>
A változók és alkalmazásuk fejezetben alapvetően arról olvashatunk, hogy a nyelv elemein milyen műveleteket végezhetünk. Erről a részről is egyértelműen tükröződik a Python nyelv tömör írásmódja. Az operátorokhoz rendelt műveletek segítségével sok esetben tömörebben leírt kódot kapunk, mint általában egy programnyelvnél. 
</para>
<para>
Az elágazás és a ciklusok szintén tömörebb megfogalmazást kínálnak. A ciklusmagban szereplő uitasításoknak mind bentebb kell kezdődni. Függvényeket a def kulcsszóval vezethetünk be. Miután bevezettünk egy függvényt vagy eljárást, azt utána bárhol meghívhatjuk. 
</para>
<para>
A nyelv támogatja az objektumorientáltságot is. Osztályokat a class kulcsszóval vezetünk be. Fontos szerepet kap a self kulcsszó is, amely az éppen aktuális példányra hivatkozik. Konstruktort a def __init__(self,...) leírásával vezetünk be.
</para>
<para>
Végül a kivételkezelést szeretném nagyvonalakban ismertetni. Erre a try-except páros ad lehetőséget. A try blokkban lévő utasításokat megkíséreljük végrehajtani, és ha hibát kapunk, nem áll le a program, hanem az else ágban szereplő utasítások hajtódnak végre. C++ nyelvben ilyen a try-catch-finally, amiről később még szó fog esni. 
</para>
<para>
Végezetül, a Python programozás kapcsán szeretném megemlíteni a Turing fejezetben megjelenő szkriptet, mint konkrét példát. A tömörségével ellentétben jelentős program azok számára, akik hobbiszinten villámokra vadásznak, vagy egyszerűen csak értesülni szeretnének a közelben tevékenykedő zivatarok megjelenéséről. A programcsipet adott időközönként letölti a debreceni térképkivágatot és a hozzá tartozó villámokat megjelenítő png képet, és ez utóbbit kielemezve hangjelzést ad, amennyiben adott távolságon belül villámlás történt. Mindez egy ilyen rövid kódcsipetbe belegyúrható.
</para>
</section>
<section>
<title>
Pici könyv
</title>
<section>
<title>
Bevezetés
</title>
<para>
Juhász István: Magas szintű programozási nyelvek 1 jegyzete mélyen lefedi az órai tematikát, és nagyon jó betekintést nyújt mind általában a programozásba, mind pedig a C nyelvbe. Rövid, tömör, lényegre törő és könnyen emészthető. 
</para>
<para>
Az első fejezetrész eleje a modellezésről szól. A valós életbeli problémák jórésze modellezhető, leegyszerűsíthető, és számtógép segítségével gyorsabban megoldható. Az egyik legelemibb példa erre a számológép.
</para>
<para>
Ezt követően a programozási nyelvek világába csöppenünk bele, alapfogalmakat definiálunk. A magasszintű nyelveknél a forráskódot futtatható, gépi nyelvű formára kell alakítani. Ezt a szerepet tölti be a fordító. A C nyelv esetén az említett fordítást előfordítás előzi meg. A fordítás során lexikális-, szintaktikai- majd szemantikai elemzés történik, végül a kódgenerálás.
</para>
<para>
A programnyelvek egyik osztályozási módja szerint a következő kategóriákat sorolhatjuk fel: imperatív nyelve, valamint deklaratív nyelvek. Ez előbbiek szorosan kötődnek a Neumann-architektúrához, ez utóbbiak kevésbé. Előbbikebe tartoznak az eljárás- és objektumorientált, utóbbiakba az applikatív- és logikai nyelvek. A máselvű nyelvek ezen két kategóriába nem sorolhatók.
</para>

</section>
<section>
<title>
A nyelv alapelemei
</title>
<para>
A programok forrásszövegének legkisebb alkotóelemei a karakterek. A karakterkészlet programozási nyelvenként eltérő, egy azonban közös: betűk, számjegyek és egyéb karakterek is jelen vannak. 
</para>
<para>
A lexikális egységek egy forráskód azon részei, amelyeket a fordító lexikális elemzés során felismer és tokenizál. Ide tartoznak a többkarakteres szimbólumok (pl. ++, /*, */), szimbolikus nevek (karakterrel kezdődő, betűvel és számjeggyel folytatódó karaktersor), címkék, megjegyzések, literálok. Ezek közül a szimbolikus nevek közé tartoznak a kulcsszavak, melyek előre definiáltak, és ilyen változóneveket nem vezethetünk be (pl. if, for). A megjegyzés készítésére kétféle lehetőség is adatik: egy adott sorba (annak végére) szeretnénk megjegyzést fűzni, vagy egy adott tartományt szeretnénk. Ezekre a "//" és a "/*, */" páros alkalmas a c nyelvben. Ezeket a sorokat a fordítóprogram nem veszi figyelembe. A literálok csoportjába különböző típusok tartoznak bele többek között a számtípusok. Felírhatjuk az egészeket decimális, oktális (0-val kezdődő), és hexadecimális (0x-el kezdődő) alakban. Továbbá lehetőségünk val a valós számok ábrázolására (lebegőpontos számtípusok a float és a double). Hosszú egész a long típus, az előjel nélküliséget az u (unsigned) jelző jelzi.
</para>
<para>
További literált képez a karakter- és sztring literál.
</para>
</section>
<section>
<title>
A forrásszöveg összeállítása
</title>
<para>
A C programnyelv a szabadformátumú nyelvek csoportjába ratrozik, ami azt jelenti, hogy egy-egy utasítást a végjelet jelentő ';' zár le. Az alacsonyabb szintű Assembly-vel elentétben ez azt jelenti, hogy egy utasítást akárhány sorba írhatok, illetve egy sorba akárhány utasítás kerülhet, a program lefut, amennyiben a végjelek pozíciója helyes.
</para>
<para>
A lexikális egységeket szóközzel választjuk el egymástól. 
</para>
</section>

<section>
<title>
Adattípusok
</title>
<para>
Az adattípus egy absztrakt programozási eszköz, melynek neve van, ami egy azonosító. A C nyelv típusos, rendelkezik beépített típusokkal. Az adattípusok mögött van egy megfelelő belső ábrázolási mód. Vannak nyelvek, amik támogatják saját típus létrehozását is (pl: C++).
</para>
<para>
Az egyszerű típusok csoportjába tartoznak a már korábban említett számtípusok, karaktertípusok, (karaktersorozatok), logikai típusok, felsorolásos (enum) típus. A C nyelvben a 0 logikai értéke hamis, minden más érték igaz. Ezt a módszert alkalmazzák többek közöztt hibák kiíratására. A sorszámozott típusnál meghatározott feltételeknek teljesülni kell.
</para>
<para>
Az összetett típusok közé sorolható a tömb típus, melyet meghatároz a dimenzióinak száma, indexkészletének típusa és tartománya, illetve az elemeinek típusa. A tömb elemeire indexekkel hivatkozhatunk, bár a könyvből kiderül, a hivatkozásra más lehetőség is adatik C nyelven. Egy tömb létrehozásakor annak darabszámát meg kell adni C-ben. A C++ nyelvnél rugalmasságot biztosít a vector típus. A másik összetett típus a rekord típus, c-ben stuktúra.
</para>
<para>
A következő csoportot a mutató típus képviseli. A mutató típus az absztrakt adatszerkezetek szétszórt reprezenzációjánál jelentős szerepű. Speciális érték a NULL érték, ekkor a mutató nem mutat sehová.
</para>
<para>
A C nyelvben lehetőség adatik nevesített konstans létrehozására is a #define kifejezéssel.
</para>
</section>

<section>
<title>
A változó
</title>
<para>
A változóknak 4 komponensük van: a név, attribútumok, cím, érték. A deklaráció lehet explicit, implicit és automatikus. A változók a leggyakrabban használt elemei egy programnak, a leggyakoribb rájuk jellemző utasítás az értékadó utasítás. C nyelven ez így írható le: változónév = kifejezés;
</para>
<para>
Az aritmetikai típusok az egyszerű típusok, rajtuk aritmetikai műveletek végezhetők. A felsorolásos típus tartomány elemei egészek (int típusúak). Enum típust alkalmazhatunk például akkor, ha színekhez számokat szeretnénk rendelni. Saját típust a typedef, struktúrát a struct, uniont a union kulcsszóval hozhatunk létre.
</para>
</section>

<section>
<title>
Kifejezések
</title>
<para>
A kifejezések szintaktikai eszközök. A kifejezések az operandusokból és az operátorokból állnak, a kerek zárójelek által pedig tetszőleges műveleti sorrend kényszeríthető. Az operátorok lehetnek unárisak, binárisak, ternálisok, annak függvényében, hány operandussal végeznek műveletet. Továbbá lehet prefix, infix és postfix egy operátor, az operandus és a rá ható operátor pozíciójának függvényében. További fogalmak a műveleti sorrend és a kötési irány, melyek a precedencia táblázatban szerepelnek.
</para>
</section>

<section>
<title>
Utasítások
</title>
<para>
A végrehajtható utasításoknak a következő csoportjait különböztetjük meg:
</para>
<para>
Az első csoport tagját képviseli az értékadó utasítás, melyről korábban volt szó.
</para>
<para>
Az üres utasításra jó példa a continue, melynek során az adott utasításblokkban, a continue kulcsszó után szereplő utasítások nem hajtódnak végre, így lépünk ki az utasításblokkból.
</para>
<para>
Ugró utasításra példa a goto a C nyelvben melynek használata ritkán indokolt.
</para>
<para>
Az elágazó utasítás a feltételes utasítás. C-beli megfelelője az if-else if-else hármas, vagy if-else páros. Többirányú változata a switch-case-default hármas. Azon ág hajtódik végre, amelyen a feltétel igaz.
</para>
<para>
A ciklusszervező utasítások csoportját a feltételes (while) ciklus, előírt lépésszámú (for) ciklus képviselik. Ez előbbi lehet elöltesztelős, amikor a program egyszer mindenképpen lefut. Végtelen ciklus alakul ki akkor, amikor a feltételre mindig igazatat kapunk. Eseményvezérelt programoknál ez lehet szándékos. Kilépni belőle a break utasítással lehetséges.
</para>
<para>
Vezérlő utasítások c-ben a continue, break és a return.
</para>
</section>


</section>
    </section>        
    <section>
        <title>Programozás bevezetés</title>
        <para>                
            <citation>KERNIGHANRITCHIE</citation>
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/zmfT9miB-jY">https://youtu.be/zmfT9miB-jY</link>
        </para>   
<para>
Konkrét programok írása nélkül nem lehet megtanulni programozni. Miután az alapfogalmakat bevezettük, néhány példán keresztül megismerjük a C nyelv tulajdonságait (avagy az elméleteti tudásunkat gyakorlatba építjük). A Kerninghan-Ritchie-féle C nyelvről szóló könyv ebben segít minket. 
</para>
<section>
<title>
Indulás
</title>
<para>
Az első fejezetben megismerkedünk a legalapvetőbb programmal, mely kiíratja a konzolablakra a "Halló mindenki!" szöveget. Ehhez egyszerűen inkludáljuk az stdio.h fejrészt, amely az input-output műveletek végrehajtását teszi lehetővé és a printf() ennek része. Szó esik egy c program fordításáról és futtatásáról is. 
</para>
<para>
A bevezetés második példájában a standard outputra egy celsius-fahrenheit átváltásról szóló táblázatot küldünk ki. Itt megismerkedünk a változók bevezetésével, egy-két típussal, illetve a while ciklus is helyet kap, ugyanis több értékre megy végbe az átváltás. Ha lebegőpontos számokkal dolgozunk, és ilyen formában íratjuk ki a számokat, pontosabb eredmény érhető el. Ezen formázási eszközök megjelennek ebben a fejezetben. 
</para>
<para>
Ezt követően ugyanazt a programot megírjuk for ciklus segítségével is. Jól látható, mennyivel egyszerűbb így a felírás. Akkor tudjuk ezt alkalmazni, ha ismerjük a határokat.
</para>
<para>
A következő részben a szimbolikus állandók szerepelnek. Létrehozásuk a következőképpen lehetséges: 
</para>
<para>
#define ALSO  0
</para>
<para>
Ekkor az ALSO szót beírva a 0 értékre hivatkozunk. 
</para>
<para>
Ahhoz, hogy némi interaktivitást belecsempésszünk a programba, lehetőség van arra is, hogy a felhasználó által bevitt számadatot konvertáljuk át. Beolvasásra a getchar, kimenetre másolásra a putchar parancsok állnak a rendelkezésünkre. A beolvasott karaktersorozat az atof ill. atoi függvények segítségével számmá alakíthatók. 
</para>
<para>
Ezt követően a karakterek, szavak és sorok számolása következik. Karakterek számolása esetén nem állítunk be elválasztót, szavak esetén a ' ', sorok esetén elválasztónak a '\n' karaktert állíthatjuk be, és az ezek közötti egységek darabszámát íratjuk ki. Itt megjelenik az if-else utasítás is.
</para>
</section>
<section>
<title>Típusok, operátorok, kifejezések, vezérléses szerkezetek</title>
<para>                
Ezen részekre részletesen nem térnék ki, ugyanis a feldolgozást a pici könyvvel párhuzamosan végeztem, és a főbb dolgokat a korábbiakban már leírtam. Az egyes alfejezetekben megjelenő példakódok jó részét kielemeztem, lefordítottam, futtattam, ezzel megerősítve az elméleti tartalmat.
</para>
</section>
<section>
<title>Utasítások</title>
<para>                
Az utasítások a leírásuk sorrendjében hajtódnak végre. Ezeknek szeretném néhány konkrét fajtáját bemutatni.
</para>
<para>                

</para>
</section>
    </section>        
    <section>
        <title>Programozás</title>
        <para>                
            <citation>BMECPP</citation>
        </para>

    <section>
    <title>
    A C++ nem objektumorientált tulajdonságai
    </title>
    <para>
    A C++ nyelv alapelemeiben nagyban hasonlít az elődjét képező C nyelvre. A fő különbség az objektumorientáltság támogatása a C++ nyelv részéről, ám további különbségek is akadnak. Jó példa a függvények visszatérési értékének különbsége. A C++-nál például nem támogatott az alap visszatérési érték típus (A C esetében int), Így ilyen függvény fordításakor hibát kapunk. 
    </para>
<para>
Azon felül, hogy a C++-ban hivatalosan is megjelent a bool, mint logikai típus, változók deklarálását is szabadabban vihetünk véghez. Így átláthatóbb kódot kaphatunk. 
</para>
<para>
Mivel a C++ nyelvben a függvényeket nemcsak a nevük, hanem paraméterlistájuk is azonosítja, lehetőségünk adatik a függvénynevek túlterhelésére. Mindez akkor lehetséges, ha különböző típusok szerepelnek a függvény paraméterlistájában. 
</para>
<para>
Arra is lehetőségünk van, hogy egy függvény paraméterlistájában alapértelmezett értékeket adjunk meg, így híváskor kevesebb paraméter beírásával is működőképes a függvényünk.
</para>
<para>
Egy függvény paraméterlistájába nem csak értéket, hanem referenciatípust is átadhatunk. Így a függvénytörzsben szereplő, az annak átadott érték megváltoztatása ténylegesen megtörténik, nem csak a másolat értékét változtatjuk.
</para>
<para>
Bevezetésként nagyvonalakban ennyit a két nyelv alapvető különbségeiről. Ezek után lépjünk be az objektumok világába!
</para>

    </section>

<section>
    <title>
    Objektumok és osztályok
    </title>
<para>
Az objektumorientált szemléletmód szükségességét a hagyományos szemléletben megírt programok átláthatatlansága, nehéz javíthatósága hívta életre. Ezen szemléletnek egyik legfontosabb alapelve az egységbe zárás (osztályokban gondolkodunk). Az osztály önmagában egy sablon, belőle objektumok képezhetők. Fontos tulajdonság továbbá az adatrejtés is. Ennek lényege, hogy be tudunk vezetni olyan változókat, amelyeket csak maga az objektum láthat. Ezeket private kulcsszóval látjuk el. Így az objektumon kívülről nem tudunk közvetlenül hivatkozni adott értékekre, vagy módosítani azokat. Privát adatot módosítani és lekérni publikus elérhetőségű függvényekkel lehetséges. Ezzel az eljárásmóddal rengeteg hiba kiküszöbölhető. 
</para>
<para>
Minden objektumpéldány létrehozható és törölhető a C++ nyelvben. Ezt a konstruktorok és destruktorok segítségével végezhetjük el. Megjegyzés: egy másik objektumorientált szemléletű nyelvnél, a Java-nál destruktor nem létezik, ott garbage collector végzi a szükségtelen objektumok törlését.
</para>
<para>

</para>
</section>



    </section>        
</chapter>                
