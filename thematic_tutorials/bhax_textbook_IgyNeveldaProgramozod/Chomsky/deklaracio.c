#include <stdio.h>


int main(int argc, char** argv) {
    
    
    //próba
    int firstvalue, secondvalue;
    int *mypointer;
    
    mypointer = &firstvalue;
    *mypointer = 10;
    
    mypointer = &secondvalue;
    *mypointer = 20;
    
    printf("Firstvalue: %d, Secondvalue: %d\n",firstvalue, secondvalue);
    
    
    /** Vezessük be a programba a következőket! */
    //egész
    int a = 30;
    
    //egészre mutató mutató
    int *b;
    b = &a;     //a b mutató, mely az a szám címére mutat
   
    
    
    //egész referenciája
    *b = a;     //a *b az a szám konkrét értékét adja vissza
    
    
    //egészek tömbje
    int et[15];     //15 elemű tömb bevezetése
    
    //egészek tömbjének referenciája (nem az első elemé)
    int (*etr)[15] = &et;   //etr megkapja ap referenciáját
           
    //egészre mutató mutatók tömbje
    int* emmt[15];      //emmt egy olyan tömb, melyben egészre mutató mutatók vannak
    
    
    //egészre mutató mutatót visszaadó függvény
    int* f();   //Az f függvény egészre mutató mutatót (int*) ad vissza.
    
    
    //egészre mutató mutatót visszaadó függvényre mutató mutató
    int** g();  
    
    //egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó,
    //egészet kapó függvény
    int* i(int h(int, int));    
    
    //Ebből:
    //int h(int,int)  két egészet kapó, egészet visszaadó függvény
    //int* i pedig egy egészet kapó, mutatót visszaadó függvény
    
    
    //függvénymutató egy egészet visszaadó és két egészet kapó függvényre 
    //mutató mutatót visszaadó, egészet kapó függvényre
    int** k(int j(int, int));
    
    //az előző függvény visszatérési értékére mutatok
    
    return 0;
}

