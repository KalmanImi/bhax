#include <stdio.h>


int
main () {
        
	/*
	for (int a = 0, a < 10, a++)
	nem fordul le a c89-es verzióban, ugyanis a for paraméterlistájában nem szerepelhet értékadás
	*/
	/*Így lefordul:*/
	int a = 0;
    for (a; a<10; a++) {}   
	const b = 10;

    /*
	// Ez egy komment 
	nem fordul le a c89-es verzióban, ugyanis ez a komment típus nem meghatározott
	*/
	/*Így lefordul*/


    signed char c;



    return 0;

}
