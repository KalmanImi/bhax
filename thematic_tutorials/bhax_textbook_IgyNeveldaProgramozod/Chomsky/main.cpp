/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: kimi
 *
 * Created on 2020. március 7., 13:02
 */

#include <cstdlib>
#include <stdio.h>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int a;     //Bevezetünk egy egész változót, de nem rendelünk hozzá értéket 
    
    int *b = &a;    //Létrehozunk egy mutatót, ami a címére mutat
    
    int &r = a;     //Az a-hoz rendelt értéket adja vissza r.
    
    int c[5] = {1,2,3,4,5};      //Bevezetünk egy 5 db integert tartalmazó tömböt
    
    int (&tr)[5] = c; //Magára a tömbre hivatkozás (nem az első elemre)
    printf("%d",tr[3]);
    
    int* d[5];  //Mutatókat tartalmazó tömb bevezetése
    
    int* h();   //Mutatót visszaadó függvény
    
    int* (*l) ();   //Mutató mutatót visszaadó függvényre
    
    int (*v (int c)) (int a, int b);     
    
    int (*(*z) (int)) (int, int);
    
    return 0;   
}

