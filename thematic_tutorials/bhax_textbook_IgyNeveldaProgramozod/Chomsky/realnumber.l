%{
#include <stdio.h>
int realnumbers = 0;
%}
digit	[0-9]
%%
{digit}*(\.{digit}+)?	{++realnumbers; 
    printf("[realnum=%f]\n", atof(yytext));}

\n {
printf("\nThe number of real numbers is %d now.\n", realnumbers);
}

%%
int
main ()
{
 yylex ();
 return 0;
}

