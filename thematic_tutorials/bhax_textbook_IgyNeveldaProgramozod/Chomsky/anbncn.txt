Generatív nyelvtan esetén adott kell legyen egy kezdőszimbólum, amelyet meghatározott szabályok egymás utáni, tetszőleges sorrendben történő alkalmazásával átalakítunk jelsorozatokká. Jelen esetben a kezdőszimbólum legyen S, az ábécéhez tartozzon az 'a', 'b' és 'c' szimbólum, avagy legyen:

NT = {S,B}
T = {a,b,c}

ahol NT a nem terminális szimbólumok halmaza, T a terminális szimbólumok halmaza. A cél, hogy  olyan nyelvtant fogalmazzunk meg, melynek hatására a kimenetben csak terminális szimbólumok legyenek. Az anbncn nyelv megadására a következő nyelvtani szabályok legyenek adottak:

1. S -> aSBc
2. S -> abc
3. cB -> Bc
4. bB -> bb

anbncn nyelv a következő szabályok alkalmazásával áll elő:

1. eset (a1b1c1):
S -2-> abc

2. eset (a2b2c2):
S -1-> a S Bc
S -2-> aab cB c
S -3-> aa bB cc
S -4->  aabbcc

3. eset ((a3b3c3):
S -1-> a S Bc
S -1-> aa S BcBc
S -3-> aaab cB cBc
S -3-> aaabB cB cc
S -3-> aaa bB Bccc
S -4-> aaabbbccc


...
