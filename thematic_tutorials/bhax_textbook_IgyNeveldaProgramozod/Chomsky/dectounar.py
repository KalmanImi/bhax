#Unáris számrendszerbe váltó Turing gép modellje

dec = ['=',0,0,0,0,0,0,0,1,1,'==']  #A számot reprezentáló tömb

hol = len(dec)-1    #A kezdőpozíció
mennyi = '' #A szám unáris értéke
r = 0       #Jobbra:1, balra:0
szr = 10    #számrendszer


def fv(dec,hol,mennyi,r):
    if (dec[hol] == '='):   #Ha elérem az elejét
        print(mennyi)       #Ez lesz a végeredmény
        return mennyi
    elif (dec[hol] == '=='):    #Ha a végére érek, megfordítom az irányt
        r = 0
        hol -= 1
        fv(dec, hol, mennyi, r)
    elif(dec[hol] == 0 and r == 0):     #Ha balra haladok és 0-n állok, egyszerűen megyek tovább
        hol -= 1
        fv(dec, hol, mennyi, r)
    elif (dec[hol] == 0 and r == 1):    #Ha jobbra megyek és 0-n állok, értékét a
        dec[hol] = szr-1                #számrendszer maximumára állítom és lépek tovább
        hol += 1
        fv(dec, hol, mennyi, r)
    elif (dec[hol] != 0 and r == 0):    #Ha balra haladok és nem nullát találok,
        dec[hol] -= 1                   #levonok belőle egyet, és jobbra lépek tovább
        mennyi += '|'
        r = 1
        hol += 1
        fv(dec, hol, mennyi, r)

#Meghívom a függvényt, mely rekurzívan hívja önmagát, amíg a számot reprezentáló tömb elejére nem ér.
fv(dec,hol,mennyi,r)
