


3.7. Logikus -- PIPA, még videó kell

A feladat megoldásának kezdetén megnéztem a 4 kifejezést, próbáltam elemezni. Az angol nyelvű logikai utasítások alapján egész jól ki lehetett következtetni, hogy milyen logikai formula szerepel a képernyőn, de ahhoz, hogy biztosra menjek, a TeXworks nevű program segítségével megnyitottam pdf formátumban a leírt nyers kifejezéseket. Próbaként először megnyitottam Bátfai Norbert tanár úr példáját, majd a konkrét feladatot oldottam meg. A kifejezések természetes nyelven így szólnak:
Minden x szám esetén létezik olyan nála nagyobb y szám, ami prím.
Minden y szám esetén létezik olyan nála nagyobb y szám, ami prím, és a rákövetkezőjének rákövetkezője is prím.
Van olyan y szám, amely ha minden x szám esetén prím, akkor x < y.
Van olyan y szám, amely ha minden x szám esetén kisebb mint y, akkor nem prím.


3.6. A források olvasása -- PIPA, még videó kell
1
2,3 Az i változónak értékül adom a 0-t, majd egyesével léptetem pozitív irányba, addig, amíg el nem érem az 5-öt. Az, hogy a ++ értéknövelés a prefixumba szerepel, vagy a változó után, for ciklus esetén nem mérvadó, viszont ha i++ értékét átadom egy másik változónak, i eredeti értékét kapom vissza, ++i esetén az eredeti i eggyel megnövelt értékét.

4 Az i változónak értékül adom a 0-t, majd egyesével léptetem pozitív irányba, addig, amíg el nem érem az 5-öt. Eközben a tomb nevű tömböt elemekkel töltöm fel, egészen a 0 indexűtől a 4-esig. Itt már van szerepe a ++i és i++ különbségének, ugyanis az első esetén tomb[0] értéke 1, a második esetében tomb[0] értéke 0 lesz. Itt nagy a hibalehetőség, ugyanis -többek között - ha 5-nél kisebb méretű tömböt adunk meg, nem fog lefutni a program.

5 Az i változónak értékül adom a 0-t, majd egyesével léptetem pozitív irányba, addig, amíg i el nem éri n-et. Eközben a *d mutató egyel történő megnöveltjéhez hozzárendelem a *s egyel való megnöveltjét. Ez utóbbi két feltétel között egy && operátor szerepel, ami teljesen értelmetlen. 

6 Ebben az esetben két decimális számot írunk ki egymás mellé, szóközzel elválasztva. Az első szám az f két paraméterrel bíró függvény eredményét adja meg a, és a rákövetkező érték esetén. A második ugyanúgy az f függvényét adja vissza eredményül, csak a paramétereket megfordítjuk. 
Próbaként megírtam egy f függvényt:
int f(int x, int y) {
    return x-y;
}
int a = 1;
Ebbe a függvénybe behelyettesítve a-t, az eredmény:
0 1

7 Két decimális számot íratunk ki a képernyőre szóközzel elválasztva. Az első az f függvény visszatérési értéke, a második a függvényparaméterként megadott a változó értéke.

8 Két decimális számot íratunk ki a képernyőre szóközzel elválasztva. Az első az f függvény visszatérési értéke, a második az a változó értéke. Függvényparaméterként az a változó referenciáját adjuk meg.

3.5. Lexer -- PIPA, még videó kell

Ehhez a feladathoz először telepítettem az előfeltételeket, majd kipróbáltam a Bátfai Norbert tanár úr által feltöltött forrást. Maga a lexelő program tulajdonképpen legenerálja a c forráskódot, amelyet a -lfl kapcsolóval lefuttatva elindul a program. A program futásakor a bemenetre írt karaktersorozat enter lenyomásakor módosul, a megadott előfeltételeknek megfelelően. A forrásban egy bemenő karakterhez többféle kimenetet is megadhatunk, sőt, karakterláncok megadása is lehetséges.
Példa a kódcsipetből az 'a' bemenet megváltoztatására:
/kód
{'a', {"4", "4", "@", "/-\\"}},
/kód

Azt, hogy a felsorolásból melyik érték rendelődik hozzá a különböző karakterekhez, az r értéke dönti el, amely egy random generált szám.

/kód
int r = 1+(int) (100.0*rand()/(RAND_MAX+1.0));
/kód

Az eredeti séma szerint nagyon nagy az első karakterek előfordulási aránya az utána következők rovására, így az r értékéhez kapcsolódó feltételeket módosítottam, ezzel bonyolítva a visszafejtés folyamatát.

/kód
if(r<25)
    printf("%s", l337d1c7[i].leet[0]);
else if(r<50)
    printf("%s", l337d1c7[i].leet[1]);
else if(r<75)
    printf("%s", l337d1c7[i].leet[2]);
else 
    printf("%s", l337d1c7[i].leet[3]);
/kód

Ezen kódolási eljárás igen régi, viszonylag könnyen visszafejthető. Annál bonyolultabb  visszanyerni az eredeti szöveget, minél több lehetséges értéket rendelünk egy karakterhez, illetve azokat minél nagyobb változatossággal alkalmazzuk. 
Ezt a példát Python programozási nyelven viszonylag egyszerűen megvalósíthatjuk. A példa kedvéért a következő kódrészletet szeretném bemutatni, melybe csupán az 'a','b','l' és 'k' karaktereket vettem bele
a feldolgozandó karakterek listájába a következőképpen:

/kód

tomb = [
    ['a', '4','@'],
    ['b', '8','13','j3'],
    ['l', '1',']['],
    ['k', '|<', '1<', 'l<', '|{'],
]

/kód

A tömbben lévő tömbök első eleme mindig az átalakítandó. Bemenetként megadtam az "ablak" szót:

/kód
string = 'ablak'
/kód

Végül egy egyszerű algoritmussal elvégezzük a karakterek átformálását:
/kód
for c in string:
    for i in tomb:
        if(i[0] == c):
            print(i[random.randint(1,len(i)-1)], end='')
/kód

A véletlen számot a random függvény importálásával nyerjük, melynek értéke mindig az adott tömb 1. (és nem 0.) elemétől a tömb hosszáig tartó kifejezésekből nyerhető ki. Ezt az értéket ki is írjuk. 4 egymást követő kimenetele a programnak, az ablak szóra:
413][4|{
4j3][@1<
@8][4l<
@131@l<

A teljes kód: hivatkozás lesz!!!
/kód
import random

tomb = [
    ['a', '4','@'],
    ['b', '8','13','j3'],
    ['l', '1',']['],
    ['k', '|<', '1<', 'l<', '|{'],
]

string = 'ablak'

for c in string:
    for i in tomb:
        if(i[0] == c):
            print(i[random.randint(1,len(i)-1)], end='')
/kód

Ennek mintájára akár felépíthető az egész abc-re és számokra is a lexer.

3.4. Valós szám (hivatkozás) PIPA, még videó kell

A valós számok kiolvasásához egy speciális programra volt szükség, amely adott utasítások szerint elkészíti nekünk a c nyelvű forráskódot. Az előfeltételek telepítése után kipróbáltam a mellékelt mintaprogramot. 
Annyi módosítást végeztem az eredeti programhoz képest, hogy a valós szám számlálót az yylex függvény belsejébe tettem. Jelen esetben akkor írja ki az eddigi valós számok számát, amikor entert ütünk. Az eredeti kódban, mivel a függvényből nem volt megadva kiugrási mód, így a programból kilépve nem írta ki a valós számok számát. A valós számokon túl egyszerű módosítással egész számokat is be tudunk olvasni:
/kód
(\-+)?{digit}* {printf("[intnum=%s %d]\n", yytext, atoi(yytext));}
/kód
Az első rész a mínusz előjel meglétének vizsgálata. Egyszerűen az atof helyett az atoi függvényt használjuk az integer érték beolvasására.

3.3. Hivatkozási nyelv

Komment stílus
függvényben deklarálás

3.2.
anbncn

3.1.

A decimálisból unárisra váltó turing gép. 



